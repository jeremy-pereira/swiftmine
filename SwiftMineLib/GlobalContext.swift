//
//  GlobalContext.swift
//  SwiftMineLib
//
//  Created by Jeremy Pereira on 23/05/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//
import Toolbox

/// Provides a context for stuff that is global to the application.
public protocol GlobalContext
{
    /// Random number generator for the game
    var rand: PRNG { get }
    /// A home groen random number generator for the game.
    var pcgRand: PRNG { get }
}
