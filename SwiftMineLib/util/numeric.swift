//
//  numeric.swift
//  SwiftMineLib
//
//  Created by Jeremy Pereira on 28/05/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//


/// Given a 3D box defined by two vectors, returns the same box but defiend
/// by the "lowest" and highest vertices.
/// - Parameters:
///   - p1: One vertex
///   - p2: Another vertex
/// - Returns: The lowest and highest vertices of the same box
func sortBoxVertices(p1: V3S16, p2: V3S16) -> (V3S16, V3S16)
{
	let r1x = min(p1.x, p2.x)
    let r2x = max(p1.x, p2.x)
    let r1y = min(p1.y, p2.y)
    let r2y = max(p1.y, p2.y)
    let r1z = min(p1.z, p2.z)
    let r2z = max(p1.z, p2.z)
    return(V3S16(x: r1x, y: r1y, z: r1z), V3S16(x: r2x, y: r2y, z: r2z))
}


/// Not sure what this does. It seems to divide the vector by the distance and
/// round toweards negative infinity.
///
/// - Parameters:
///   - position: Position to get the container pos of
///   - distnce: Distance to divide by
/// - Returns: A new position
func container(position: V3S16, distance: Int) -> V3S16
{
    return V3S16(x: container(position: position.x, distance: distance),
                 y: container(position: position.y, distance: distance),
                 z: container(position: position.z, distance: distance))
}

private func container(position: Int16, distance: Int) -> Int16
{
	let shortDist = Int16(distance)
    return (position >= 0 ? position : (position - shortDist + 1)) / shortDist
}
