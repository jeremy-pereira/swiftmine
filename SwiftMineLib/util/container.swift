//
//  container.swift
//  SwiftMineLib
//
//  Created by Jeremy Pereira on 28/05/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//

fileprivate class Queue<T>
{
    var count: Int = 0
    class Entry
    {
        // The pointers towards the front of the queue are weak to avoid
        // ownership cycles.
        weak var prev: Entry?
        var next: Entry?
        let value: T
        unowned let owner: Queue<T>

        init(owner: Queue<T>, value: T)
        {
            self.value = value
            self.owner = owner
        }
    }
    var first: Entry?
    weak var last: Entry?

    func removeAll()
    {
        first = nil
        last = nil
        count = 0
    }

    func remove(entry: Entry)
    {
        guard entry.owner === self else { fatalError("Attempt to remove queue entry from the wrong queue") }

		if let prev = entry.prev
        {
            prev.next = entry.next
        }
        else
        {
            first = entry.next
        }
        if let next = entry.next
        {
            next.prev = entry.prev
        }
        else
        {
            last = entry.prev
        }
        entry.prev = nil
        entry.next = nil
        count -= 1
    }

    func addFirst(_ key: T) -> Entry
    {
		let entry = Entry(owner: self, value: key)
        if let first = first
        {
            first.prev = entry
            entry.next = self.first
            self.first = entry
        }
        else
        {
            assert(last == nil)
            first = entry
            last = entry
        }
        count += 1
        return entry
    }

    func removeLast() -> Entry?
    {
        guard let ret = last else { return nil }
        assert (ret.next == nil)

		if let prev = ret.prev
        {
            prev.next = nil
            last = prev
        }
        else
        {
            last = nil
            first = nil
        }
        count -= 1
        ret.prev = nil
        return ret
    }
}


/// A least recently used cache
public class LRUCache<Key: Hashable, Value>
{
    let limit: Int
    let cacheMiss: (Key) throws -> Value?


    /// Initialise the cache
    ///
    /// - Parameters:
    ///   - limit: The limit to the number of cache entries, must be more than 0
    ///   - cacheMiss: Function for getting values that are not in the cache
    
    public init(limit: Int, cacheMiss: @escaping (Key) throws -> Value?)
    {
        guard limit > 0 else { fatalError("Cache limit must be positive") }

		self.limit = limit
        self.cacheMiss = cacheMiss
    }

    private var lruQueue: Queue = Queue<Key>()

    private class CacheEntry
    {
        fileprivate let value: Value
        fileprivate var lruEntry: Queue<Key>.Entry

        init(value: Value, lruEntry: Queue<Key>.Entry)
        {
            self.value = value
            self.lruEntry = lruEntry
        }
    }

    private var map: [Key : CacheEntry] = [:]

	public func invalidate()
    {
		lruQueue.removeAll()
        map.removeAll()
    }

    func lookup(_ key: Key) throws -> Value?
	{
        let ret: Value?
		if let cacheEntry = map[key]
        {
            // Got a hit. Retrieve the value and move the cache entry to the
            // front of the queue.
			ret = cacheEntry.value
            lruQueue.remove(entry: cacheEntry.lruEntry)
            cacheEntry.lruEntry = lruQueue.addFirst(key)
        }
        else
        {
			if let newValue = try cacheMiss(key)
            {
                // Add a new cache entry for the new value
                ret = newValue
                let cacheEntry = LRUCache.CacheEntry(value: newValue,
                                                  lruEntry: lruQueue.addFirst(key))
                map[key] = cacheEntry
                while lruQueue.count > limit
                {
                    if let lruEntry = lruQueue.removeLast()
                    {
                        map[lruEntry.value] = nil
                    }
                }
            }
            else
            {
                ret = nil
            }
        }
        return ret
    }
}
