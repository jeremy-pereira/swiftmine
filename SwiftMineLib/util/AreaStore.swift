//
//  AreaStore.swift
//  SwiftMineLib
//
//  Created by Jeremy Pereira on 25/05/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//
import Toolbox

private let log = Logger.getLogger("SwiftMineLib.util.AreaStore")

/// Represents an area
class Area: Codable
{
    var id: UInt32 = UInt32.max
    let minEdge: V3S16
    let maxEdge: V3S16

    init(minEdge: V3S16, maxEdge: V3S16)
    {
        (self.minEdge, self.maxEdge) = sortBoxVertices(p1: minEdge, p2: maxEdge)
    }
    var data: String = ""

    /// True if the area contains the point.
    ///
    /// - Parameter point: Point to check if it contains
    /// - Returns: true if the area contains the point
    func contains(point: V3S16) -> Bool
    {
        let min = self.minEdge <= point
        let max = point <= self.maxEdge
        return  min && max
    }


    /// True if this area contains the other area
    ///
    /// - Parameter area: The other area
    /// - Returns: true if this area contains the other area.
    func contains(area: Area) -> Bool
    {
		return self.contains(point: area.minEdge) && self.contains(point: area.maxEdge)
    }

    func overlaps(_ area: Area, inDimension: (V3S16) -> Int16) -> Bool
    {
		return !(inDimension(self.minEdge) > inDimension(area.maxEdge)
        		|| inDimension(self.maxEdge) < inDimension(area.minEdge))
    }

    /// True if this area overlaps the given area.
    ///
    /// - Parameter area: Other area
    /// - Returns: true if this area overlaps the other area
    func overlaps(_ area: Area) -> Bool
    {
        return self.overlaps(area, inDimension: { $0.x} )
        	&& self.overlaps(area, inDimension: { $0.y} )
            && self.overlaps(area, inDimension: { $0.z} )
    }
}

/// A store for areas I guess
class AreaStore: Codable
{
    enum CodingKeys: String, CodingKey
    {
		case nextId
        case areaMap
        case cacheIsEnabled
        case cacheBlockRadius

    }

    enum Err: Error
    {
		case duplicateAreaId
    }

    fileprivate init()
    {
        resourceCache = LRUCache(limit: 1000)
        {
            [unowned self] key in
            let radius = Int16(self.cacheBlockRadius)
			let minEdge = key * radius
            let maxEdge = minEdge + Vector3D(x: radius, y: radius, z: radius)
            return self.areasInArea(minEdge: minEdge, maxEdge: maxEdge, acceptOverlap: true)
        }
    }

    private var nextId: UInt32  = 0

    fileprivate func getNextId() -> UInt32
    {
        defer { nextId += 1 }
        return nextId
    }

    /// Nunber of areas in the store. This is the same as the `size` member in
    /// MineTest
    var count: Int { return areaMap.count }

    /// Reserve space for more areas. This is currently a no-op
    ///
    /// - Parameter count: The number of things to reserve space for.
    func reserve(count: Int) {}

    fileprivate var areaMap: [UInt32 : Area] = [:]

    func insert(area: Area) throws
    {
        fatalError("\(#function) needs to be overridden")
    }

    func areasInArea(minEdge: V3S16, maxEdge: V3S16, acceptOverlap: Bool) -> [Area]
    {
        fatalError("\(#function) needs to be overridden")
    }

    fileprivate func invalidateCache()
    {
		if cacheIsEnabled
        {
            resourceCache.invalidate()
        }
    }
    private var cacheIsEnabled = true
    private var cacheBlockRadius = 64
    private var resourceCache: LRUCache<V3S16, [Area]>!


    /// Find the areas for a position
    ///
    /// - Parameter position: The position to search for
    /// - Returns: List of areas found for the position
    func areasFor(position: V3S16) -> [Area]
    {
        let ret: [Area]
        if cacheIsEnabled
        {
            let mBlock = container(position: position, distance: cacheBlockRadius)
            do
            {
                guard let preList = try resourceCache.lookup(mBlock) else { return [] }
                ret = preList.filter{ $0.contains(point: position) }
           }
            catch
            {
                log.warn("Error looking up position: \(error)")
                return []
            }
        }
        else
        {
            ret = areasForImpl(position: position)
        }
        return ret
    }


    /// Override to find areas
    ///
    /// - Parameter position: The position of the area
    /// - Returns: TThe areas containing the position
    func areasForImpl(position: V3S16) -> [Area]
    {
        fatalError("\(#function) needs to be overridden")
    }

    /// Remove the area with the given id
    ///
    /// - Parameter id: The id of the area to go
    func removeArea(id: UInt32) -> Bool
    {
        fatalError("\(#function) not implemented")
    }
}


/// An area store that uses vectors to define where things are.
class VectorAreaStore: AreaStore
{
    override init()
    {
        super.init()
    }

    required init(from: Decoder) throws
    {
        try super.init(from: from)
        areas = Array(areaMap.values)
    }

    private var areas: [Area] = []

    override func insert(area: Area) throws
    {
        if area.id == UInt32.max
        {
			area.id = getNextId()
        }
		guard areaMap[area.id] == nil else { throw Err.duplicateAreaId }
        areaMap[area.id] = area
		areas.append(area)
        invalidateCache()
    }

    override func areasInArea(minEdge: V3S16, maxEdge: V3S16, acceptOverlap: Bool) -> [Area]
    {
        let searchArea = Area(minEdge: minEdge, maxEdge: maxEdge)
        return areas.filter
        {
            return acceptOverlap ? searchArea.overlaps($0) : searchArea.contains(area: $0)
        }
    }

    override func removeArea(id: UInt32) -> Bool
    {

        guard areaMap[id] != nil else { return false }
        guard let indexToGo = areas.index(where: { $0.id == id })
        else { fatalError("Area in areaMap but not areas") }
		areas.remove(at: indexToGo)
        areaMap.removeValue(forKey: id)
        invalidateCache()
        return true
    }
}


/// An area store that uses some sort of spatial thing
class SpatialAreaStore: AreaStore
{
    override init()
    {
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    // TODO: Implement this
}
