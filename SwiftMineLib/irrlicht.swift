//
//  irrlicht.swift
//  SwiftMineLib
//
//  Created by Jeremy Pereira on 25/05/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//


/// Protocols to which components of a vector must conform
public typealias Vector3DComponent = Hashable & SignedNumeric & Codable

/// Objects conforming to this protocol can have various rotate operations
/// performed on them
public protocol Rotatable
{
    /// Rotate 90 degrees around the z access so that the y axis becones the
    /// x axis
    ///
    /// - Returns: The object rotated 90 degrees clockwise round the z axis
	func rotatedYToX() -> Self
    /// Rotate 90 degrees around the y access so that the z axis becones the
    /// x axis
    ///
    /// - Returns: The object rotated 90 degrees anticlockwise round the y axis
    func rotatedZToX() -> Self
}

/// Objects conforming to this protocol can have various reflect operations
/// performed on them
public protocol Reflectable
{
    /// Reflect around the y = x line so that the y axis becones the
    /// x axis
    ///
    /// - Returns: The object rotated 90 degrees clockwise round the z axis
    func reflectedYToX() -> Self
    /// Reflect around the z = x line so that the z axis becones the
    /// x axis
    ///
    /// - Returns: The object rotated 90 degrees anticlockwise round the y axis
    func reflectedZToX() -> Self
}

public typealias Transformable = Rotatable & Reflectable

/// A 3D vector structure that replaces the Irrlicht `vector3d` type.
public struct Vector3D<T: Vector3DComponent>: Codable
{
    let x: T
    let y: T
    let z: T

    init(x: T, y: T, z: T)
    {
        self.x = x
        self.y = y
        self.z = z
    }

    public static func * (left: Vector3D<T>, scale: T) -> Vector3D<T>
    {
        return Vector3D(x: left.x * scale, y: left.y * scale, z: left.z * scale)
    }

    public static func + (left: Vector3D<T>, right: Vector3D<T>) -> Vector3D<T>
    {
		return Vector3D(x: left.x + right.x, y: left.y + right.y, z: left.z + right.z)
    }
    public static func - (left: Vector3D<T>, right: Vector3D<T>) -> Vector3D<T>
    {
        return Vector3D(x: left.x - right.x, y: left.y - right.y, z: left.z - right.z)
    }

}

extension Vector3D: CustomStringConvertible
{
    public var description: String
    {
        return "(\(x), \(y), \(z))"
    }
}

extension Vector3D: Hashable
{
    public static func == (left: Vector3D, right: Vector3D) -> Bool
    {
        return left.x == right.x && left.y == right.y && left.z == right.z
    }

    public var hashValue: Int { return x.hashValue ^ y.hashValue ^ z.hashValue }
}

extension Vector3D where T: Comparable
{
    /// A point is `<=` another point if all of its components are `<=` the
    /// compoents of the other point
    ///
    /// - Parameters:
    ///   - lhs: The first point
    ///   - rhs: The second point
    /// - Returns: true if all componetns of `lhs` are `<=` all components of
    ///            `rhs`
    public static func <= (lhs: Vector3D<T>, rhs: Vector3D<T>) -> Bool
    {
        return lhs.x <= rhs.x && lhs.y <= rhs.y && lhs.z <= rhs.z
    }
}

extension Vector3D: Transformable
{
    public func reflectedYToX() -> Vector3D<T>
    {
        return Vector3D(x: self.y, y: self.x, z: self.z)
    }

    public func reflectedZToX() -> Vector3D<T>
    {
        return Vector3D(x: self.z, y: self.y, z: self.x)
    }

    public func rotatedZToX() -> Vector3D<T>
    {
        return Vector3D(x: self.z, y: self.y, z: -self.x)
    }

    public func rotatedYToX() -> Vector3D<T>
    {
        return Vector3D(x: self.y, y: -self.x, z: self.z)
    }
}

typealias V3S16 = Vector3D<Int16>


/// An axis aligned bounding box in 3D space. Based on the Irrlicht type
/// `aabbox3d`. Has methods to support  occlusion culling or clipping
public struct AABBox3D<T: Vector3DComponent>
{
    public let minEdge: Vector3D<T>
    public let maxEdge: Vector3D<T>

    public init(minEdge: Vector3D<T>, maxEdge: Vector3D<T>)
    {
		self.minEdge = minEdge
        self.maxEdge = maxEdge
    }
    public init(minX: T, minY: T, minZ: T, maxX: T, maxY: T, maxZ: T)
    {
        self.init(minEdge: Vector3D(x: minX, y: minY, z: minZ),
                  maxEdge: Vector3D(x: maxX, y: maxY, z: maxZ))
    }
}

extension AABBox3D: CustomStringConvertible
{
    public var description: String
    {
        return "AABBox3D<from \(minEdge) to \(maxEdge)>"
    }
}

extension AABBox3D: Transformable
{
    public func reflectedYToX() -> AABBox3D<T>
    {
        return AABBox3D(minEdge: minEdge.reflectedYToX(), maxEdge: maxEdge.reflectedYToX())
    }

    public func reflectedZToX() -> AABBox3D<T>
    {
        return AABBox3D(minEdge: minEdge.reflectedZToX(), maxEdge: maxEdge.reflectedZToX())
    }

    public func rotatedZToX() -> AABBox3D<T>
    {
        return AABBox3D(minEdge: minEdge.rotatedZToX(), maxEdge: maxEdge.rotatedZToX())
    }

    public func rotatedYToX() -> AABBox3D<T>
    {
        return AABBox3D(minEdge: minEdge.rotatedYToX(), maxEdge: maxEdge.rotatedYToX())
    }
}

public typealias AABBox3DF = AABBox3D<Float>
