//
//  GameDef.swift
//  SwiftMineLib
//
//  Created by Jeremy Pereira on 23/05/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//


/// Game definition protocol
protocol GameDef
{
//    // These are thread-safe IF they are not edited while running threads.
//    // Thus, first they are set up and then they are only read.
//    virtual IItemDefManager* getItemDefManager()=0;
//    virtual INodeDefManager* getNodeDefManager()=0;
//    virtual ICraftDefManager* getCraftDefManager()=0;
//
//    // Used for keeping track of names/ids of unknown nodes
//    virtual u16 allocateUnknownNodeId(const std::string &name)=0;
//
//    virtual MtEventManager* getEventManager()=0;
//
//    // Only usable on the server, and NOT thread-safe. It is usable from the
//    // environment thread.
//    virtual IRollbackManager* getRollbackManager() { return NULL; }
//
//    // Shorthands
//    IItemDefManager  *idef()     { return getItemDefManager(); }
//    INodeDefManager  *ndef()     { return getNodeDefManager(); }
//    ICraftDefManager *cdef()     { return getCraftDefManager(); }
//
//    MtEventManager   *event()    { return getEventManager(); }
//    IRollbackManager *rollback() { return getRollbackManager(); }
//
//    virtual const std::vector<ModSpec> &getMods() const = 0;
//    virtual const ModSpec* getModSpec(const std::string &modname) const = 0;
//    virtual std::string getWorldPath() const { return ""; }
//    virtual std::string getModStoragePath() const = 0;
//    virtual bool registerModStorage(ModMetadata *storage) = 0;
//    virtual void unregisterModStorage(const std::string &name) = 0;

}
