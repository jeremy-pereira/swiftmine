//
//  Settings.swift
//  SwiftMine
//
//  Created by Jeremy Pereira on 16/05/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//

//
//#ifndef SETTINGS_HEADER
//#define SETTINGS_HEADER
//
//#include "irrlichttypes_bloated.h"
//#include "util/string.h"
//#include "threading/mutex.h"
//#include <string>
//#include "util/cpp11_container.h"
//#include <list>
//#include <set>
//
//class Settings;
//struct NoiseParams;
//
//// Global objects
//extern Settings *g_settings;
//extern std::string g_settings_path;
//
//// Type for a settings changed callback function
//typedef void (*SettingsChangedCallback)(const std::string &name, void *data);
//
//typedef std::vector<
//std::pair<
//SettingsChangedCallback,
//void *
//>
//> SettingsCallbackList;
//
//typedef UNORDERED_MAP<std::string, SettingsCallbackList> SettingsCallbackMap;
//
/// Defines possible options
public typealias OptionList = [String : ValueSpec]


/// Types of value for a setting
///
/// - string: The value is a string
/// - flag: The value is an on/off flag
public enum ValueType
{
    case string
    case flag
}
//
//
//struct ValueSpec {
//    ValueSpec(ValueType a_type, const char *a_help=NULL)
//    {
//    type = a_type;
//    help = a_help;
//    }
//
//    ValueType type;
//    const char *help;
//};

public struct ValueSpec
{
    public let type: ValueType
    public let help: String

    public init(type: ValueType, help: String)
    {
        self.type = type
        self.help = help
    }
}

/// A settings object to carry user settings.
public struct Settings
{
    /// Entry from settings
    ///
    /// - flag: The entry is an on/off entry
    /// - string: The entry value is a string
    /// - group: The entry is a group
    public indirect enum Entry
    {
        case flag(Bool)
        case string(String)
        case double(Double)
        case int(Int)
        case group([String : Entry])
    }
    private var entries: [String : Entry] = [:]
    private var defaults: [String : Entry] = [:]
    /// Initialise an empty settings
    public init() {}


    /// Populate the default settings. This is everything from
    /// `default_settings.cpp`
    public mutating func populateDefaults()
    {
		// client and server
        defaults["language"] = .string("")
        defaults["name"] = .string("")
        defaults["bind_address"] = .string("")
        defaults["serverlist_url"] = .string("servers.minetest.net")

        // Client only
        defaults["address"] = .string("")
        defaults["enable_sound"] = .flag(true)
        defaults["sound_volume"] = .double(0.8)
        defaults["enable_mesh_cache"] = .flag(false)
        defaults["mesh_generation_interval"] = .int(0)
        defaults["mesh_block_cache_size"] = .int(20)
        defaults["enable_vbo"] = .flag(true)
        defaults["free_move"] = .flag(false)
        // TODO: Populate the rest of the defaults
//        settings->setDefault("fast_move", "false");
//        settings->setDefault("noclip", "false");
//        settings->setDefault("screenshot_path", ".");
//        settings->setDefault("screenshot_format", "png");
//        settings->setDefault("screenshot_quality", "0");
//        settings->setDefault("client_unload_unused_data_timeout", "600");
//        settings->setDefault("client_mapblock_limit", "5000");
//        settings->setDefault("enable_build_where_you_stand", "false" );
//        settings->setDefault("send_pre_v25_init", "false");
//        settings->setDefault("curl_timeout", "5000");
//        settings->setDefault("curl_parallel_limit", "8");
//        settings->setDefault("curl_file_download_timeout", "300000");
//        settings->setDefault("curl_verify_cert", "true");
//        settings->setDefault("enable_remote_media_server", "true");
//        settings->setDefault("enable_client_modding", "false");
//        settings->setDefault("max_out_chat_queue_size", "20");
//
//        // Keymap
//        settings->setDefault("remote_port", "30000");
//        settings->setDefault("keymap_forward", "KEY_KEY_W");
//        settings->setDefault("keymap_autorun", "");
//        settings->setDefault("keymap_backward", "KEY_KEY_S");
//        settings->setDefault("keymap_left", "KEY_KEY_A");
//        settings->setDefault("keymap_right", "KEY_KEY_D");
//        settings->setDefault("keymap_jump", "KEY_SPACE");
//        settings->setDefault("keymap_sneak", "KEY_LSHIFT");
//        settings->setDefault("keymap_drop", "KEY_KEY_Q");
//        settings->setDefault("keymap_zoom", "KEY_KEY_Z");
//        settings->setDefault("keymap_inventory", "KEY_KEY_I");
//        settings->setDefault("keymap_special1", "KEY_KEY_E");
//        settings->setDefault("keymap_chat", "KEY_KEY_T");
//        settings->setDefault("keymap_cmd", "/");
//        settings->setDefault("keymap_cmd_local", ".");
//        settings->setDefault("keymap_minimap", "KEY_F9");
//        settings->setDefault("keymap_console", "KEY_F10");
//        settings->setDefault("keymap_rangeselect", "KEY_KEY_R");
//        settings->setDefault("keymap_freemove", "KEY_KEY_K");
//        settings->setDefault("keymap_fastmove", "KEY_KEY_J");
//        settings->setDefault("keymap_noclip", "KEY_KEY_H");
//        settings->setDefault("keymap_hotbar_next", "KEY_KEY_N");
//        settings->setDefault("keymap_hotbar_previous", "KEY_KEY_B");
//        settings->setDefault("keymap_mute", "KEY_KEY_M");
//        settings->setDefault("keymap_increase_volume", "");
//        settings->setDefault("keymap_decrease_volume", "");
//        settings->setDefault("keymap_cinematic", "");
//        settings->setDefault("keymap_toggle_hud", "KEY_F1");
//        settings->setDefault("keymap_toggle_chat", "KEY_F2");
//        settings->setDefault("keymap_toggle_force_fog_off", "KEY_F3");
//        settings->setDefault("keymap_toggle_update_camera", "");
//        settings->setDefault("keymap_toggle_debug", "KEY_F5");
//        settings->setDefault("keymap_toggle_profiler", "KEY_F6");
//        settings->setDefault("keymap_camera_mode", "KEY_F7");
//        settings->setDefault("keymap_screenshot", "KEY_F12");
//        settings->setDefault("keymap_increase_viewing_range_min", "+");
//        settings->setDefault("keymap_decrease_viewing_range_min", "-");
//        // Some (temporary) keys for debugging
//        settings->setDefault("keymap_print_debug_stacks", "KEY_KEY_P");
//        settings->setDefault("keymap_quicktune_prev", "KEY_HOME");
//        settings->setDefault("keymap_quicktune_next", "KEY_END");
//        settings->setDefault("keymap_quicktune_dec", "KEY_NEXT");
//        settings->setDefault("keymap_quicktune_inc", "KEY_PRIOR");
//
//        settings->setDefault("show_debug", "false");

//        settings->setDefault("fsaa", "0");
//        settings->setDefault("undersampling", "0");
//        settings->setDefault("enable_fog", "true");
//        settings->setDefault("fog_start", "0.4");
//        settings->setDefault("3d_mode", "none");
//        settings->setDefault("3d_paralax_strength", "0.025");
//        settings->setDefault("tooltip_show_delay", "400");
//        settings->setDefault("zoom_fov", "15");
//        settings->setDefault("fps_max", "60");
//        settings->setDefault("pause_fps_max", "20");
//        settings->setDefault("viewing_range", "100");
//        settings->setDefault("screenW", "800");
//        settings->setDefault("screenH", "600");
//        settings->setDefault("autosave_screensize", "true");
//        settings->setDefault("fullscreen", "false");
//        settings->setDefault("fullscreen_bpp", "24");
//        settings->setDefault("vsync", "false");
//        settings->setDefault("fov", "72");
//        settings->setDefault("leaves_style", "fancy");
//        settings->setDefault("connected_glass", "false");
//        settings->setDefault("smooth_lighting", "true");
//        settings->setDefault("display_gamma", "2.2");
//        settings->setDefault("texture_path", "");
//        settings->setDefault("shader_path", "");
//        settings->setDefault("video_driver", "opengl");
//        settings->setDefault("cinematic", "false");
//        settings->setDefault("camera_smoothing", "0");
//        settings->setDefault("cinematic_camera_smoothing", "0.7");
//        settings->setDefault("enable_clouds", "true");
//        settings->setDefault("view_bobbing_amount", "1.0");
//        settings->setDefault("fall_bobbing_amount", "0.0");
//        settings->setDefault("enable_3d_clouds", "true");
//        settings->setDefault("cloud_height", "120");
//        settings->setDefault("cloud_radius", "12");
//        settings->setDefault("menu_clouds", "true");
//        settings->setDefault("opaque_water", "false");
//        settings->setDefault("console_height", "1.0");
//        settings->setDefault("console_color", "(0,0,0)");
//        settings->setDefault("console_alpha", "200");
//        settings->setDefault("selectionbox_color", "(0,0,0)");
//        settings->setDefault("selectionbox_width", "2");
//        settings->setDefault("node_highlighting", "box");
//        settings->setDefault("crosshair_color", "(255,255,255)");
//        settings->setDefault("crosshair_alpha", "255");
//        settings->setDefault("hud_scaling", "1.0");
//        settings->setDefault("gui_scaling", "1.0");
//        settings->setDefault("gui_scaling_filter", "false");
//        settings->setDefault("gui_scaling_filter_txr2img", "true");
//        settings->setDefault("desynchronize_mapblock_texture_animation", "true");
//        settings->setDefault("hud_hotbar_max_width", "1.0");
//        settings->setDefault("enable_local_map_saving", "false");
//        settings->setDefault("show_entity_selectionbox", "true");
//        settings->setDefault("texture_clean_transparent", "false");
//        settings->setDefault("texture_min_size", "64");
//        settings->setDefault("ambient_occlusion_gamma", "2.2");
//        settings->setDefault("enable_shaders", "true");
//        settings->setDefault("enable_particles", "true");
//
//        settings->setDefault("enable_minimap", "true");
//        settings->setDefault("minimap_shape_round", "true");
//        settings->setDefault("minimap_double_scan_height", "true");
//
//        // Effects
//        settings->setDefault("directional_colored_fog", "true");
//        settings->setDefault("inventory_items_animations", "false");
//        settings->setDefault("mip_map", "false");
//        settings->setDefault("anisotropic_filter", "false");
//        settings->setDefault("bilinear_filter", "false");
//        settings->setDefault("trilinear_filter", "false");
//        settings->setDefault("tone_mapping", "false");
//        settings->setDefault("enable_bumpmapping", "false");
//        settings->setDefault("enable_parallax_occlusion", "false");
//        settings->setDefault("generate_normalmaps", "false");
//        settings->setDefault("normalmaps_strength", "0.6");
//        settings->setDefault("normalmaps_smooth", "1");
//        settings->setDefault("parallax_occlusion_mode", "1");
//        settings->setDefault("parallax_occlusion_iterations", "4");
//        settings->setDefault("parallax_occlusion_scale", "0.08");
//        settings->setDefault("parallax_occlusion_bias", "0.04");
//        settings->setDefault("enable_waving_water", "false");
//        settings->setDefault("water_wave_height", "1.0");
//        settings->setDefault("water_wave_length", "20.0");
//        settings->setDefault("water_wave_speed", "5.0");
//        settings->setDefault("enable_waving_leaves", "false");
//        settings->setDefault("enable_waving_plants", "false");
//
//
//        // Input
//        settings->setDefault("invert_mouse", "false");
//        settings->setDefault("mouse_sensitivity", "0.2");
//        settings->setDefault("repeat_rightclick_time", "0.25");
//        settings->setDefault("random_input", "false");
//        settings->setDefault("aux1_descends", "false");
//        settings->setDefault("doubletap_jump", "false");
//        settings->setDefault("always_fly_fast", "true");
//        settings->setDefault("continuous_forward", "false");
//        settings->setDefault("enable_joysticks", "false");
//        settings->setDefault("joystick_id", "0");
//        settings->setDefault("joystick_type", "");
//        settings->setDefault("repeat_joystick_button_time", "0.17");
//        settings->setDefault("joystick_frustum_sensitivity", "170");
//
//        // Main menu
//        settings->setDefault("main_menu_path", "");
//        settings->setDefault("main_menu_mod_mgr", "1");
//        settings->setDefault("main_menu_game_mgr", "0");
//        settings->setDefault("modstore_download_url", "https://forum.minetest.net/media/");
//        settings->setDefault("modstore_listmods_url", "https://forum.minetest.net/mmdb/mods/");
//        settings->setDefault("modstore_details_url", "https://forum.minetest.net/mmdb/mod/*/");
//        settings->setDefault("serverlist_file", "favoriteservers.txt");
//
//        #if USE_FREETYPE
//        settings->setDefault("freetype", "true");
//        settings->setDefault("font_path", porting::getDataPath("fonts" DIR_DELIM "Arimo-Regular.ttf"));
//        settings->setDefault("font_shadow", "1");
//        settings->setDefault("font_shadow_alpha", "127");
//        settings->setDefault("mono_font_path", porting::getDataPath("fonts" DIR_DELIM "Cousine-Regular.ttf"));
//        settings->setDefault("fallback_font_path", porting::getDataPath("fonts" DIR_DELIM "DroidSansFallbackFull.ttf"));
//
//        settings->setDefault("fallback_font_shadow", "1");
//        settings->setDefault("fallback_font_shadow_alpha", "128");
//
//        std::string font_size_str = std::to_string(TTF_DEFAULT_FONT_SIZE);
//
//        settings->setDefault("fallback_font_size", font_size_str);
//        #else
//        settings->setDefault("freetype", "false");
//        settings->setDefault("font_path", porting::getDataPath("fonts" DIR_DELIM "mono_dejavu_sans"));
//        settings->setDefault("mono_font_path", porting::getDataPath("fonts" DIR_DELIM "mono_dejavu_sans"));
//
//        std::string font_size_str = std::to_string(DEFAULT_FONT_SIZE);
//        #endif
//        settings->setDefault("font_size", font_size_str);
//        settings->setDefault("mono_font_size", font_size_str);
//
//
//        // Server
//        settings->setDefault("disable_escape_sequences", "false");
//
//        // Network
//        settings->setDefault("enable_ipv6", "true");
//        settings->setDefault("ipv6_server", "false");
//        settings->setDefault("workaround_window_size","5");
//        settings->setDefault("max_packets_per_iteration","1024");
//        settings->setDefault("port", "30000");
//        settings->setDefault("strict_protocol_version_checking", "false");
//        settings->setDefault("player_transfer_distance", "0");
//        settings->setDefault("max_simultaneous_block_sends_per_client", "10");
//        settings->setDefault("max_simultaneous_block_sends_server_total", "40");
//        settings->setDefault("time_send_interval", "5");
//
//        settings->setDefault("default_game", "minetest");
//        settings->setDefault("motd", "");
//        settings->setDefault("max_users", "15");
//        settings->setDefault("creative_mode", "false");
//        settings->setDefault("show_statusline_on_connect", "true");
//        settings->setDefault("enable_damage", "true");
//        settings->setDefault("default_password", "");
//        settings->setDefault("default_privs", "interact, shout");
//        settings->setDefault("enable_pvp", "true");
//        settings->setDefault("disallow_empty_password", "false");
//        settings->setDefault("disable_anticheat", "false");
//        settings->setDefault("enable_rollback_recording", "false");
//        #ifdef NDEBUG
//        settings->setDefault("deprecated_lua_api_handling", "legacy");
//        #else
//        settings->setDefault("deprecated_lua_api_handling", "log");
//        #endif
//
//        settings->setDefault("kick_msg_shutdown", "Server shutting down.");
//        settings->setDefault("kick_msg_crash", "This server has experienced an internal error. You will now be disconnected.");
//        settings->setDefault("ask_reconnect_on_crash", "false");
//
//        settings->setDefault("profiler_print_interval", "0");
//        settings->setDefault("active_object_send_range_blocks", "3");
//        settings->setDefault("active_block_range", "3");
//        //settings->setDefault("max_simultaneous_block_sends_per_client", "1");
//        // This causes frametime jitter on client side, or does it?
//        settings->setDefault("max_block_send_distance", "9");
//        settings->setDefault("block_send_optimize_distance", "4");
//        settings->setDefault("server_side_occlusion_culling", "true");
//        settings->setDefault("max_clearobjects_extra_loaded_blocks", "4096");
//        settings->setDefault("time_speed", "72");
//        settings->setDefault("server_unload_unused_data_timeout", "29");
//        settings->setDefault("max_objects_per_block", "64");
//        settings->setDefault("server_map_save_interval", "5.3");
//        settings->setDefault("chat_message_max_size", "500");
//        settings->setDefault("chat_message_limit_per_10sec", "8.0");
//        settings->setDefault("chat_message_limit_trigger_kick", "50");
//        settings->setDefault("sqlite_synchronous", "2");
//        settings->setDefault("full_block_send_enable_min_time_from_building", "2.0");
//        settings->setDefault("dedicated_server_step", "0.1");
//        settings->setDefault("active_block_mgmt_interval", "2.0");
//        settings->setDefault("abm_interval", "1.0");
//        settings->setDefault("nodetimer_interval", "0.2");
//        settings->setDefault("ignore_world_load_errors", "false");
//        settings->setDefault("remote_media", "");
//        settings->setDefault("debug_log_level", "action");
//        settings->setDefault("emergequeue_limit_total", "256");
//        settings->setDefault("emergequeue_limit_diskonly", "32");
//        settings->setDefault("emergequeue_limit_generate", "32");
//        settings->setDefault("num_emerge_threads", "1");
//        settings->setDefault("secure.enable_security", "true");
//        settings->setDefault("secure.trusted_mods", "");
//        settings->setDefault("secure.http_mods", "");
//
//        // Physics
//        settings->setDefault("movement_acceleration_default", "3");
//        settings->setDefault("movement_acceleration_air", "2");
//        settings->setDefault("movement_acceleration_fast", "10");
//        settings->setDefault("movement_speed_walk", "4");
//        settings->setDefault("movement_speed_crouch", "1.35");
//        settings->setDefault("movement_speed_fast", "20");
//        settings->setDefault("movement_speed_climb", "3");
//        settings->setDefault("movement_speed_jump", "6.5");
//        settings->setDefault("movement_liquid_fluidity", "1");
//        settings->setDefault("movement_liquid_fluidity_smooth", "0.5");
//        settings->setDefault("movement_liquid_sink", "10");
//        settings->setDefault("movement_gravity", "9.81");
//
//        // Liquids
//        settings->setDefault("liquid_loop_max", "100000");
//        settings->setDefault("liquid_queue_purge_time", "0");
//        settings->setDefault("liquid_update", "1.0");
//
//        // Mapgen
//        settings->setDefault("mg_name", "v7");
//        settings->setDefault("water_level", "1");
//        settings->setDefault("mapgen_limit", "31000");
//        settings->setDefault("chunksize", "5");
//        settings->setDefault("mg_flags", "dungeons");
//        settings->setDefault("fixed_map_seed", "");
//        settings->setDefault("max_block_generate_distance", "7");
//        settings->setDefault("enable_mapgen_debug_info", "false");
//
//        // Server list announcing
//        settings->setDefault("server_announce", "false");
//        settings->setDefault("server_url", "");
//        settings->setDefault("server_address", "");
//        settings->setDefault("server_name", "");
//        settings->setDefault("server_description", "");
//
//        settings->setDefault("high_precision_fpu", "true");
//        settings->setDefault("enable_console", "false");
//
//        settings->setDefault("screen_dpi", "72");
    }

    public mutating func readConfigFile(filepath: URL) throws
    {
        let configFileString = try String(contentsOf: filepath, encoding: .utf8)
        var lines = configFileString.split(separator: "\n").makeIterator()
        entries = try parseConfigLines(content: &lines)
    }

    private typealias LineIterator = IndexingIterator<Array<String.SubSequence>>

    private enum ParseEvent
    {
        case none
        case invalid
        case comment
        case kvPair(key: String, value: String)
        case end
        case group(name: String)
        case multiLine(name: String)
    }

    private func parseConfigLines(content: inout LineIterator,
                                      end: String = "") throws -> [String : Entry]
    {
        var ret: [String : Entry] = [:]
        while let line = content.next()
        {
            switch parseConfigObject(line: line, end: end)
            {
            case .none, .invalid, .comment:
                break
            case .kvPair(key: let key, value: let value):
                ret[key] = .string(value)
            case .end:
                return ret
            case .group(name: let name):
                let theGroup = try parseConfigLines(content: &content, end: "}")
                ret[name] = .group(theGroup)
            case .multiLine(name: let name):
                ret[name] = .string(getMultiLine(content: &content))
            }
        }
        return ret
    }

    private func parseConfigObject(line: String.SubSequence, end: String) -> ParseEvent
    {
        let trimmedLine = line.trimmingCharacters(in: CharacterSet.whitespaces)
        if trimmedLine == ""
        {
            return ParseEvent.none
        }
        else if trimmedLine.hasPrefix("#")
        {
            return ParseEvent.comment
        }
        else if trimmedLine.hasPrefix(end)
        {
            return ParseEvent.end
        }
        let kvPair = trimmedLine.split(separator: "=")
        guard kvPair.count == 2 else { return ParseEvent.invalid }
        let key = kvPair[0].trimmingCharacters(in: CharacterSet.whitespaces)
        let value = kvPair[1].trimmingCharacters(in: CharacterSet.whitespaces)
        if value == "{"
        {
            return ParseEvent.group(name: key)
        }
        else if value == "\"\"\""
        {
            return ParseEvent.multiLine(name: key)
        }
        else
        {
            return ParseEvent.kvPair(key: key, value: value)
        }
    }

    private func getMultiLine(content: inout LineIterator) -> String
    {
        var ret = ""
        while let line = content.next(), line != "\"\"\""
        {
            ret += line
            ret += "\n"
        }
        return ret
    }
//
//    Settings & operator += (const Settings &other);
//    Settings & operator = (const Settings &other);
//
//    /***********************
//     * Reading and writing *
//     ***********************/
//
//    // Read configuration file.  Returns success.
//    bool readConfigFile(const char *filename);
//    //Updates configuration file.  Returns success.
//    bool updateConfigFile(const char *filename);
//    // NOTE: Types of allowed_options are ignored.  Returns success.
//    bool parseCommandLine(int argc, char *argv[],
//    std::map<std::string, ValueSpec> &allowed_options);
//    void writeLines(std::ostream &os, u32 tab_depth=0) const;
//
//    SettingsParseEvent parseConfigObject(const std::string &line,
//    const std::string &end, std::string &name, std::string &value);
//    bool updateConfigObject(std::istream &is, std::ostream &os,
//    const std::string &end, u32 tab_depth=0);
//
//    static bool checkNameValid(const std::string &name);
//    static bool checkValueValid(const std::string &value);
//    static std::string getMultiline(std::istream &is, size_t *num_lines=NULL);
//    static void printEntry(std::ostream &os, const std::string &name,
//    const SettingsEntry &entry, u32 tab_depth=0);
//
//    /***********
//     * Setters *
//     ***********/
//
//    // N.B. Groups not allocated with new must be set to NULL in the settings
//    // tree before object destruction.
//    bool setEntry(const std::string &name, const void *entry,
//    bool set_group, bool set_default);
//    bool set(const std::string &name, const std::string &value);
//    bool setDefault(const std::string &name, const std::string &value);
//    bool setGroup(const std::string &name, Settings *group);
//    bool setGroupDefault(const std::string &name, Settings *group);
//    bool setBool(const std::string &name, bool value);
//    bool setS16(const std::string &name, s16 value);
//    bool setU16(const std::string &name, u16 value);
//    bool setS32(const std::string &name, s32 value);
//    bool setU64(const std::string &name, u64 value);
//    bool setFloat(const std::string &name, float value);
//    bool setV2F(const std::string &name, v2f value);
//    bool setV3F(const std::string &name, v3f value);
//    bool setFlagStr(const std::string &name, u32 flags,
//    const FlagDesc *flagdesc, u32 flagmask);
//    bool setNoiseParams(const std::string &name, const NoiseParams &np,
//    bool set_default=false);
//    // N.B. if setStruct() is used to write a non-POD aggregate type,
//    // the behavior is undefined.
//    bool setStruct(const std::string &name, const std::string &format, void *value);
//
//    // remove a setting
//    bool remove(const std::string &name);
//    void clear();
//    void clearDefaults();
//    void updateValue(const Settings &other, const std::string &name);
//    void update(const Settings &other);
//
//    void registerChangedCallback(const std::string &name,
//    SettingsChangedCallback cbf, void *userdata = NULL);
//    void deregisterChangedCallback(const std::string &name,
//    SettingsChangedCallback cbf, void *userdata = NULL);
//
//    private:
//    void updateNoLock(const Settings &other);
//    void clearNoLock();
//    void clearDefaultsNoLock();
//
//    void doCallbacks(const std::string &name) const;
//
//    SettingEntries m_settings;
//    SettingEntries m_defaults;
//
//    SettingsCallbackMap m_callbacks;
//
//    mutable Mutex m_callback_mutex;
//
//    // All methods that access m_settings/m_defaults directly should lock this.
//    mutable Mutex m_mutex;

//    static Settings main_settings;
//    Settings *g_settings = &main_settings;
//    std::string g_settings_path;
//
//    Settings::~Settings()
//    {
//    clear();
//    }
//
//
//    Settings & Settings::operator += (const Settings &other)
//    {
//    update(other);
//
//    return *this;
//    }
//
//
//    Settings & Settings::operator = (const Settings &other)
//    {
//    if (&other == this)
//    return *this;
//
//    MutexAutoLock lock(m_mutex);
//    MutexAutoLock lock2(other.m_mutex);
//
//    clearNoLock();
//    updateNoLock(other);
//
//    return *this;
//    }
//
//
//    bool Settings::checkNameValid(const std::string &name)
//    {
//    bool valid = name.find_first_of("=\"{}#") == std::string::npos;
//    if (valid) valid = trim(name) == name;
//    if (!valid) {
//    errorstream << "Invalid setting name \"" << name << "\""
//    << std::endl;
//    return false;
//    }
//    return true;
//    }
//
//
//    bool Settings::checkValueValid(const std::string &value)
//    {
//    if (value.substr(0, 3) == "\"\"\"" ||
//    value.find("\n\"\"\"") != std::string::npos) {
//    errorstream << "Invalid character sequence '\"\"\"' found in"
//    " setting value!" << std::endl;
//    return false;
//    }
//    return true;
//    }
//
//
//    size_t len = value.size();
//    if (len)
//    value.erase(len - 1);
//
//    if (num_lines)
//    *num_lines = lines;
//
//    return value;
//    }

//
//
//    void Settings::writeLines(std::ostream &os, u32 tab_depth) const
//    {
//    MutexAutoLock lock(m_mutex);
//
//    for (SettingEntries::const_iterator it = m_settings.begin();
//    it != m_settings.end(); ++it)
//    printEntry(os, it->first, it->second, tab_depth);
//    }
//
//
//    void Settings::printEntry(std::ostream &os, const std::string &name,
//    const SettingsEntry &entry, u32 tab_depth)
//    {
//    for (u32 i = 0; i != tab_depth; i++)
//    os << "\t";
//
//    if (entry.is_group) {
//    os << name << " = {\n";
//
//    entry.group->writeLines(os, tab_depth + 1);
//
//    for (u32 i = 0; i != tab_depth; i++)
//    os << "\t";
//    os << "}\n";
//    } else {
//    os << name << " = ";
//
//    if (entry.value.find('\n') != std::string::npos)
//    os << "\"\"\"\n" << entry.value << "\n\"\"\"\n";
//    else
//    os << entry.value << "\n";
//    }
//    }
//
//
//    bool Settings::updateConfigObject(std::istream &is, std::ostream &os,
//    const std::string &end, u32 tab_depth)
//    {
//    SettingEntries::const_iterator it;
//    std::set<std::string> present_entries;
//    std::string line, name, value;
//    bool was_modified = false;
//    bool end_found = false;
//
//    // Add any settings that exist in the config file with the current value
//    // in the object if existing
//    while (is.good() && !end_found) {
//    std::getline(is, line);
//    SettingsParseEvent event = parseConfigObject(line, end, name, value);
//
//    switch (event) {
//    case SPE_END:
//    os << line << (is.eof() ? "" : "\n");
//    end_found = true;
//    break;
//    case SPE_MULTILINE:
//    value = getMultiline(is);
//    /* FALLTHROUGH */
//    case SPE_KVPAIR:
//    it = m_settings.find(name);
//    if (it != m_settings.end() &&
//    (it->second.is_group || it->second.value != value)) {
//    printEntry(os, name, it->second, tab_depth);
//    was_modified = true;
//    } else {
//    os << line << "\n";
//    if (event == SPE_MULTILINE)
//    os << value << "\n\"\"\"\n";
//    }
//    present_entries.insert(name);
//    break;
//    case SPE_GROUP:
//    it = m_settings.find(name);
//    if (it != m_settings.end() && it->second.is_group) {
//    os << line << "\n";
//    sanity_check(it->second.group != NULL);
//    was_modified |= it->second.group->updateConfigObject(is, os,
//    "}", tab_depth + 1);
//    } else {
//    printEntry(os, name, it->second, tab_depth);
//    was_modified = true;
//    }
//    present_entries.insert(name);
//    break;
//    default:
//    os << line << (is.eof() ? "" : "\n");
//    break;
//    }
//    }
//
//    // Add any settings in the object that don't exist in the config file yet
//    for (it = m_settings.begin(); it != m_settings.end(); ++it) {
//    if (present_entries.find(it->first) != present_entries.end())
//    continue;
//
//    printEntry(os, it->first, it->second, tab_depth);
//    was_modified = true;
//    }
//
//    return was_modified;
//    }
//
//
//    bool Settings::updateConfigFile(const char *filename)
//    {
//    MutexAutoLock lock(m_mutex);
//
//    std::ifstream is(filename);
//    std::ostringstream os(std::ios_base::binary);
//
//    bool was_modified = updateConfigObject(is, os, "");
//    is.close();
//
//    if (!was_modified)
//    return true;
//
//    if (!fs::safeWriteToFile(filename, os.str())) {
//    errorstream << "Error writing configuration file: \""
//    << filename << "\"" << std::endl;
//    return false;
//    }
//
//    return true;
//    }
//
//

    /// Errors that can be thrown from invalid settings
    ///
    /// - invalidOption: generic invalid option
    /// - unknownOption: The option is not recognised
    /// - missingOptionValue: The option should take a value but there isn't one
    public enum Err: Error
    {
        case invalidOption
        case unknownOption(String)
        case missingOptionValue(String)
    }

    /// Add the settings from the array of arguments
    ///
    /// - Parameters:
    ///   - commandLine: The command line arguments to add. `arguments[0]` is
    ///                  the program name
    ///   - allowedOptions: Options that are allowed to be specified on the
    ///                     command line.
    /// - Throws: If we fail to parse the command line correctly.
    public mutating func parse(commandLine: [String], allowedOptions: OptionList) throws
    {
        var i = commandLine.makeIterator()
        var nonOptIndex = 0
		while let opt = i.next()
        {
            // Non option argument, goes in the settings with a key of `nonopt<x>`
            // where x is the index of the non opt
			if !opt.hasPrefix("--")
            {
                guard !opt.hasPrefix("-") else { throw Err.invalidOption }
                let name = "nonopt\(nonOptIndex)"
                nonOptIndex += 1
                entries[name] = .string(opt)
            }
            else
            {
                let name = String(opt.suffix(from: opt.index(opt.startIndex, offsetBy: 2)))
				guard let valueSpec = allowedOptions[name]
                else
                {
                    throw Err.unknownOption(name)
                }
                switch valueSpec.type
                {
                case .flag:
                    entries[name] = .flag(true)
                case .string:
                    guard let optionValue = i.next()
                        else { throw Err.missingOptionValue(name) }
                    entries[name] = .string(optionValue)
                }
            }
        }
    }
//
//    /***********
//     * Getters *
//     ***********/
//
//
//    const SettingsEntry &Settings::getEntry(const std::string &name) const
//    {
//    MutexAutoLock lock(m_mutex);
//
//    SettingEntries::const_iterator n;
//    if ((n = m_settings.find(name)) == m_settings.end()) {
//    if ((n = m_defaults.find(name)) == m_defaults.end())
//    throw SettingNotFoundException("Setting [" + name + "] not found.");
//    }
//    return n->second;
//    }
//
//
//    Settings *Settings::getGroup(const std::string &name) const
//    {
//    const SettingsEntry &entry = getEntry(name);
//    if (!entry.is_group)
//    throw SettingNotFoundException("Setting [" + name + "] is not a group.");
//    return entry.group;
//    }
//
//
    /// Get the value as a string. If the key does not exist or the value is
    /// not something that can be converted to a string, `nil` is returned.
    /// This method is roughly equivalent to `Settings::get(std::string)`
    ///
    /// - Parameter key: The key to get
    /// - Returns: the string value or nil
    public func getAsString(key: String) -> String?
    {
        guard let entry = entries[key] else { return nil }
        switch entry {
        case .flag:
            return nil
        case .group:
            return nil
        case .double(let value):
            return "\(value)"
        case .int(let value):
            return "\(value)"
        case .string(let ret):
            return ret
        }
    }

//    const std::string &Settings::get(const std::string &name) const
//    {
//    const SettingsEntry &entry = getEntry(name);
//    if (entry.is_group)
//    throw SettingNotFoundException("Setting [" + name + "] is a group.");
//    return entry.value;
//    }
//
//
//    bool Settings::getBool(const std::string &name) const
//    {
//    return is_yes(get(name));
//    }
//
//
//    u16 Settings::getU16(const std::string &name) const
//    {
//    return stoi(get(name), 0, 65535);
//    }
//
//
//    s16 Settings::getS16(const std::string &name) const
//    {
//    return stoi(get(name), -32768, 32767);
//    }
//
//
//    s32 Settings::getS32(const std::string &name) const
//    {
//    return stoi(get(name));
//    }
//
//
//    float Settings::getFloat(const std::string &name) const
//    {
//    return stof(get(name));
//    }
//
//
//    u64 Settings::getU64(const std::string &name) const
//    {
//    u64 value = 0;
//    std::string s = get(name);
//    std::istringstream ss(s);
//    ss >> value;
//    return value;
//    }
//
//
//    v2f Settings::getV2F(const std::string &name) const
//    {
//    v2f value;
//    Strfnd f(get(name));
//    f.next("(");
//    value.X = stof(f.next(","));
//    value.Y = stof(f.next(")"));
//    return value;
//    }
//
//
//    v3f Settings::getV3F(const std::string &name) const
//    {
//    v3f value;
//    Strfnd f(get(name));
//    f.next("(");
//    value.X = stof(f.next(","));
//    value.Y = stof(f.next(","));
//    value.Z = stof(f.next(")"));
//    return value;
//    }
//
//
//    u32 Settings::getFlagStr(const std::string &name, const FlagDesc *flagdesc,
//    u32 *flagmask) const
//    {
//    std::string val = get(name);
//    return std::isdigit(val[0])
//    ? stoi(val)
//    : readFlagString(val, flagdesc, flagmask);
//    }
//
//
//    // N.B. if getStruct() is used to read a non-POD aggregate type,
//    // the behavior is undefined.
//    bool Settings::getStruct(const std::string &name, const std::string &format,
//    void *out, size_t olen) const
//    {
//    std::string valstr;
//
//    try {
//    valstr = get(name);
//    } catch (SettingNotFoundException &e) {
//    return false;
//    }
//
//    if (!deSerializeStringToStruct(valstr, format, out, olen))
//    return false;
//
//    return true;
//    }
//
//
//    bool Settings::getNoiseParams(const std::string &name, NoiseParams &np) const
//    {
//    return getNoiseParamsFromGroup(name, np) || getNoiseParamsFromValue(name, np);
//    }
//
//
//    bool Settings::getNoiseParamsFromValue(const std::string &name,
//    NoiseParams &np) const
//    {
//    std::string value;
//
//    if (!getNoEx(name, value))
//    return false;
//
//    Strfnd f(value);
//
//    np.offset   = stof(f.next(","));
//    np.scale    = stof(f.next(","));
//    f.next("(");
//    np.spread.X = stof(f.next(","));
//    np.spread.Y = stof(f.next(","));
//    np.spread.Z = stof(f.next(")"));
//    f.next(",");
//    np.seed     = stoi(f.next(","));
//    np.octaves  = stoi(f.next(","));
//    np.persist  = stof(f.next(","));
//
//    std::string optional_params = f.next("");
//    if (optional_params != "")
//    np.lacunarity = stof(optional_params);
//
//    return true;
//    }
//
//
//    bool Settings::getNoiseParamsFromGroup(const std::string &name,
//    NoiseParams &np) const
//    {
//    Settings *group = NULL;
//
//    if (!getGroupNoEx(name, group))
//    return false;
//
//    group->getFloatNoEx("offset",      np.offset);
//    group->getFloatNoEx("scale",       np.scale);
//    group->getV3FNoEx("spread",        np.spread);
//    group->getS32NoEx("seed",          np.seed);
//    group->getU16NoEx("octaves",       np.octaves);
//    group->getFloatNoEx("persistence", np.persist);
//    group->getFloatNoEx("lacunarity",  np.lacunarity);
//
//    np.flags = 0;
//    if (!group->getFlagStrNoEx("flags", np.flags, flagdesc_noiseparams))
//    np.flags = NOISE_FLAG_DEFAULTS;
//
//    return true;
//    }
//
//
//    bool Settings::exists(const std::string &name) const
//    {
//    MutexAutoLock lock(m_mutex);
//
//    return (m_settings.find(name) != m_settings.end() ||
//    m_defaults.find(name) != m_defaults.end());
//    }
//
//
//    std::vector<std::string> Settings::getNames() const
//    {
//    std::vector<std::string> names;
//    for (SettingEntries::const_iterator i = m_settings.begin();
//    i != m_settings.end(); ++i) {
//    names.push_back(i->first);
//    }
//    return names;
//    }
//
//
//
//    /***************************************
//     * Getters that don't throw exceptions *
//     ***************************************/
//
//    bool Settings::getEntryNoEx(const std::string &name, SettingsEntry &val) const
//    {
//    try {
//    val = getEntry(name);
//    return true;
//    } catch (SettingNotFoundException &e) {
//    return false;
//    }
//    }
//
//
//    bool Settings::getGroupNoEx(const std::string &name, Settings *&val) const
//    {
//    try {
//    val = getGroup(name);
//    return true;
//    } catch (SettingNotFoundException &e) {
//    return false;
//    }
//    }
//
//
//    bool Settings::getNoEx(const std::string &name, std::string &val) const
//    {
//    try {
//    val = get(name);
//    return true;
//    } catch (SettingNotFoundException &e) {
//    return false;
//    }
//    }
//
//

    /// True if the flag is set
    ///
    /// - Parameter flag: The flag to check
    /// - Returns: true if the flag is set
    public func hasFlag(_ flag: String) -> Bool
    {
        let ret: Bool
        if let value = entries[flag]
        {
            switch value
            {
            case .flag(let bool):
                ret = bool
            default:
				ret = false
            }
        }
        else
        {
            ret = false
        }
        return ret
    }
//    bool Settings::getFlag(const std::string &name) const
//    {
//    try {
//    return getBool(name);
//    } catch(SettingNotFoundException &e) {
//    return false;
//    }
//    }
//
//
//    bool Settings::getFloatNoEx(const std::string &name, float &val) const
//    {
//    try {
//    val = getFloat(name);
//    return true;
//    } catch (SettingNotFoundException &e) {
//    return false;
//    }
//    }
//
//
//    bool Settings::getU16NoEx(const std::string &name, u16 &val) const
//    {
//    try {
//    val = getU16(name);
//    return true;
//    } catch (SettingNotFoundException &e) {
//    return false;
//    }
//    }
//
//
//    bool Settings::getS16NoEx(const std::string &name, s16 &val) const
//    {
//    try {
//    val = getS16(name);
//    return true;
//    } catch (SettingNotFoundException &e) {
//    return false;
//    }
//    }
//
//
//    bool Settings::getS32NoEx(const std::string &name, s32 &val) const
//    {
//    try {
//    val = getS32(name);
//    return true;
//    } catch (SettingNotFoundException &e) {
//    return false;
//    }
//    }
//
//
//    bool Settings::getU64NoEx(const std::string &name, u64 &val) const
//    {
//    try {
//    val = getU64(name);
//    return true;
//    } catch (SettingNotFoundException &e) {
//    return false;
//    }
//    }
//
//
//    bool Settings::getV2FNoEx(const std::string &name, v2f &val) const
//    {
//    try {
//    val = getV2F(name);
//    return true;
//    } catch (SettingNotFoundException &e) {
//    return false;
//    }
//    }
//
//
//    bool Settings::getV3FNoEx(const std::string &name, v3f &val) const
//    {
//    try {
//    val = getV3F(name);
//    return true;
//    } catch (SettingNotFoundException &e) {
//    return false;
//    }
//    }
//
//
//    // N.B. getFlagStrNoEx() does not set val, but merely modifies it.  Thus,
//    // val must be initialized before using getFlagStrNoEx().  The intention of
//    // this is to simplify modifying a flags field from a default value.
//    bool Settings::getFlagStrNoEx(const std::string &name, u32 &val,
//    FlagDesc *flagdesc) const
//    {
//    try {
//    u32 flags, flagmask;
//
//    flags = getFlagStr(name, flagdesc, &flagmask);
//
//    val &= ~flagmask;
//    val |=  flags;
//
//    return true;
//    } catch (SettingNotFoundException &e) {
//    return false;
//    }
//    }
//
//
//    /***********
//     * Setters *
//     ***********/
//
//    bool Settings::setEntry(const std::string &name, const void *data, bool set_group, bool set_default)
//    {
//    	Settings *old_group = NULL;
//
//    	if (!checkNameValid(name))
//    		return false;
//    	if (!set_group && !checkValueValid(*(const std::string *)data))
//    		return false;
//
//    	{
//    		MutexAutoLock lock(m_mutex);
//
//    		SettingsEntry &entry = set_default ? m_defaults[name] : m_settings[name];
//    		old_group = entry.group;
//
//    		entry.value    = set_group ? "" : *(const std::string *)data;
//    		entry.group    = set_group ? *(Settings **)data : NULL;
//    		entry.is_group = set_group;
//   	}
//
//    	delete old_group;
//
//   	return true;
//    }
//
//
//    bool Settings::set(const std::string &name, const std::string &value)
//    {
//    if (!setEntry(name, &value, false, false))
//    return false;
//
//    doCallbacks(name);
//    return true;
//    }
//
//
//    bool Settings::setDefault(const std::string &name, const std::string &value)
//    {
//    return setEntry(name, &value, false, true);
//    }
//
//
//    bool Settings::setGroup(const std::string &name, Settings *group)
//    {
//    return setEntry(name, &group, true, false);
//    }
//
//
//    bool Settings::setGroupDefault(const std::string &name, Settings *group)
//    {
//    return setEntry(name, &group, true, true);
//    }
//
//
//    bool Settings::setBool(const std::string &name, bool value)
//    {
//    return set(name, value ? "true" : "false");
//    }
//
//
//    bool Settings::setS16(const std::string &name, s16 value)
//    {
//    return set(name, itos(value));
//    }
//
//
//    bool Settings::setU16(const std::string &name, u16 value)
//    {
//    return set(name, itos(value));
//    }
//
//
//    bool Settings::setS32(const std::string &name, s32 value)
//    {
//    return set(name, itos(value));
//    }
//
//
//    bool Settings::setU64(const std::string &name, u64 value)
//    {
//    std::ostringstream os;
//    os << value;
//    return set(name, os.str());
//    }
//
//
//    bool Settings::setFloat(const std::string &name, float value)
//    {
//    return set(name, ftos(value));
//    }
//
//
//    bool Settings::setV2F(const std::string &name, v2f value)
//    {
//    std::ostringstream os;
//    os << "(" << value.X << "," << value.Y << ")";
//    return set(name, os.str());
//    }
//
//
//    bool Settings::setV3F(const std::string &name, v3f value)
//    {
//    std::ostringstream os;
//    os << "(" << value.X << "," << value.Y << "," << value.Z << ")";
//    return set(name, os.str());
//    }
//
//
//    bool Settings::setFlagStr(const std::string &name, u32 flags,
//    const FlagDesc *flagdesc, u32 flagmask)
//    {
//    return set(name, writeFlagString(flags, flagdesc, flagmask));
//    }
//
//
//    bool Settings::setStruct(const std::string &name, const std::string &format,
//    void *value)
//    {
//    std::string structstr;
//    if (!serializeStructToString(&structstr, format, value))
//    return false;
//
//    return set(name, structstr);
//    }
//
//
//    bool Settings::setNoiseParams(const std::string &name,
//    const NoiseParams &np, bool set_default)
//    {
//    Settings *group = new Settings;
//
//    group->setFloat("offset",      np.offset);
//    group->setFloat("scale",       np.scale);
//    group->setV3F("spread",        np.spread);
//    group->setS32("seed",          np.seed);
//    group->setU16("octaves",       np.octaves);
//    group->setFloat("persistence", np.persist);
//    group->setFloat("lacunarity",  np.lacunarity);
//    group->setFlagStr("flags",     np.flags, flagdesc_noiseparams, np.flags);
//
//    return setEntry(name, &group, true, set_default);
//    }
//
//
//    bool Settings::remove(const std::string &name)
//    {
//    MutexAutoLock lock(m_mutex);
//
//    SettingEntries::iterator it = m_settings.find(name);
//    if (it != m_settings.end()) {
//    delete it->second.group;
//    m_settings.erase(it);
//    return true;
//    } else {
//    return false;
//    }
//    }
//
//
//    void Settings::clear()
//    {
//    MutexAutoLock lock(m_mutex);
//    clearNoLock();
//    }
//
//    void Settings::clearDefaults()
//    {
//    MutexAutoLock lock(m_mutex);
//    clearDefaultsNoLock();
//    }
//
//    void Settings::updateValue(const Settings &other, const std::string &name)
//    {
//    if (&other == this)
//    return;
//
//    MutexAutoLock lock(m_mutex);
//
//    try {
//    std::string val = other.get(name);
//    m_settings[name] = val;
//    } catch (SettingNotFoundException &e) {
//    }
//    }
//
//
//    void Settings::update(const Settings &other)
//    {
//    if (&other == this)
//    return;
//
//    MutexAutoLock lock(m_mutex);
//    MutexAutoLock lock2(other.m_mutex);
//
//    updateNoLock(other);
//    }
//
//    void Settings::updateNoLock(const Settings &other)
//    {
//    m_settings.insert(other.m_settings.begin(), other.m_settings.end());
//    m_defaults.insert(other.m_defaults.begin(), other.m_defaults.end());
//    }
//
//
//    void Settings::clearNoLock()
//    {
//
//    for (SettingEntries::const_iterator it = m_settings.begin();
//    it != m_settings.end(); ++it)
//    delete it->second.group;
//    m_settings.clear();
//
//    clearDefaultsNoLock();
//    }
//
//    void Settings::clearDefaultsNoLock()
//    {
//    for (SettingEntries::const_iterator it = m_defaults.begin();
//    it != m_defaults.end(); ++it)
//    delete it->second.group;
//    m_defaults.clear();
//    }
//
//
//    void Settings::registerChangedCallback(const std::string &name,
//    SettingsChangedCallback cbf, void *userdata)
//    {
//    MutexAutoLock lock(m_callback_mutex);
//    m_callbacks[name].push_back(std::make_pair(cbf, userdata));
//    }
//
//    void Settings::deregisterChangedCallback(const std::string &name,
//    SettingsChangedCallback cbf, void *userdata)
//    {
//    MutexAutoLock lock(m_callback_mutex);
//    SettingsCallbackMap::iterator it_cbks = m_callbacks.find(name);
//
//    if (it_cbks != m_callbacks.end()) {
//    SettingsCallbackList &cbks = it_cbks->second;
//
//    SettingsCallbackList::iterator position =
//    std::find(cbks.begin(), cbks.end(), std::make_pair(cbf, userdata));
//
//    if (position != cbks.end())
//    cbks.erase(position);
//    }
//    }
//
//    void Settings::doCallbacks(const std::string &name) const
//    {
//    MutexAutoLock lock(m_callback_mutex);
//
//    SettingsCallbackMap::const_iterator it_cbks = m_callbacks.find(name);
//    if (it_cbks != m_callbacks.end()) {
//    SettingsCallbackList::const_iterator it;
//    for (it = it_cbks->second.begin(); it != it_cbks->second.end(); ++it)
//    (it->first)(name, it->second);
//    }
//    }

}
