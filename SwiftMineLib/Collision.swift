//
//  Collision.swift
//  SwiftMineLib
//
//  Created by Jeremy Pereira on 08/06/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//


/// Represents a collision between two boxes
struct Collision
{
    // This is for some bizarre adjustment thing in x and z directions
    private static let collZero = Float(0)

    /// Direction in which collision can occur
    ///
    /// - x: Collision in x direction
    /// - y: Collision in y direction
    /// - z: Collision in z direction
	public enum Direction
    {
        case x
        case y
        case z
    }

    /// The direction of this collision
    let direction: Direction
    /// The time after which the collision will occur
    let deltaTime: Float


    /// Calculate whether a collision has occurred or not.
    ///
    /// - Parameters:
    ///   - staticBox: The box that is not moving
    ///   - movingBox: The box that is moving
    ///   - velocity: The velocity of the moving box
    ///   - delta: How close do the boxes have to be A collision if one occurs
    /// - Returns: The collision or nil if no collision occurred
    static func axisAlignedCollision(staticBox: AABBox3DF,
                                     movingBox: AABBox3DF,
                                      velocity: Vector3D<Float>,
                                         delta: Float) -> Collision?
    {
        let staticSize = staticBox.maxEdge - staticBox.minEdge
        let relBox = AABBox3DF(minEdge: movingBox.minEdge - staticBox.minEdge,
                               maxEdge: movingBox.maxEdge - staticBox.minEdge)

        if let collision = checkXAxisCollision(staticSize: staticSize,
                                                   relBox: relBox,
                                                 velocity: velocity,
                                                    delta: delta)
        {
			return collision
        }
        if let collision = checkXAxisCollision(staticSize: staticSize.reflectedYToX(),
                                                   relBox: relBox.reflectedYToX(),
                                                 velocity: velocity.reflectedYToX(),
                                                    delta: delta)
        {
            return collision?.reflectedYToX()
        }
        if let collision = checkXAxisCollision(staticSize: staticSize.reflectedZToX(),
                                               relBox: relBox.reflectedZToX(),
                                               velocity: velocity.reflectedZToX(),
                                               delta: delta)
        {
            return collision?.reflectedZToX()
        }
		return nil
    }


    /// Test for a collision in the x axis helper function
    ///
    /// - Parameters:
    ///   - staticSize: Size of the non moving box
    ///   - relBox: Box describing the difference between the static box and the
    ///             moving box
    ///   - velocity: velocity of the moving box
    ///   - delta: How close do the boxes need to be or something.
    /// - Returns: An optional containing an optional collision. If the first
    ///            level optional is nil, we couldn't determine that there
    ///            wasn't a collision in another plane.
    private static func checkXAxisCollision(staticSize: Vector3D<Float>,
                                                relBox: AABBox3DF,
                                              velocity: Vector3D<Float>,
                                                 delta: Float) -> Collision??
    {
        if velocity.x > 0
        {
            if relBox.maxEdge.x <= delta
            {
                let dTime = -relBox.maxEdge.x / velocity.x
                if (relBox.minEdge.y + velocity.y * dTime < staticSize.y)
                    && (relBox.maxEdge.y + velocity.y * dTime > collZero)
                    && (relBox.minEdge.z + velocity.z * dTime < staticSize.z)
                    && (relBox.maxEdge.z + velocity.z * dTime > collZero)
                {
                    return Collision(direction: .x, deltaTime: dTime)
                }
            }
            else if relBox.minEdge.x > staticSize.x
            {
                return .some(nil)
            }
        }
        else if velocity.x < 0
        {
            if relBox.minEdge.x >= staticSize.x - delta
            {
                let dTime = (staticSize.x - relBox.minEdge.x) / velocity.x
                if     (relBox.minEdge.y + velocity.y * dTime < staticSize.y)
                    && (relBox.maxEdge.y + velocity.y * dTime > collZero)
                    && (relBox.minEdge.z + velocity.z * dTime < staticSize.z)
                    && (relBox.maxEdge.z + velocity.z * dTime > collZero)
                {
                    return Collision(direction: .x, deltaTime: dTime)
                }
            }
            else if relBox.maxEdge.x < 0
            {
                return .some(nil)
            }
        }
		return nil
    }
}

extension Collision.Direction: Reflectable
{
    func reflectedYToX() -> Collision.Direction
    {
        switch self
        {
        case .x: return .y
        case .y: return .x
        default: return self
        }
    }

    func reflectedZToX() -> Collision.Direction
    {
        switch self
        {
        case .x: return .z
        case .z: return .x
        default: return self
        }
    }
}

extension Collision: Reflectable
{
    func reflectedYToX() -> Collision
    {
        return Collision(direction: self.direction.reflectedYToX(), deltaTime: self.deltaTime)
    }

    func reflectedZToX() -> Collision
    {
        return Collision(direction: self.direction.reflectedZToX(), deltaTime: self.deltaTime)
    }
}
