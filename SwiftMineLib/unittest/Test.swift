//
//  Test.swift
//  SwiftMineLib
//
//  Created by Jeremy Pereira on 23/05/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//
import Toolbox
import Foundation

private let log = Logger.getLogger("SwiftMineLib.unittest.Test")

struct TestGameDef: GameDef
{
//    TestGameDef();
//    ~TestGameDef();
//
//    IItemDefManager *getItemDefManager() { return m_itemdef; }
//    INodeDefManager *getNodeDefManager() { return m_nodedef; }
//    ICraftDefManager *getCraftDefManager() { return m_craftdef; }
//    ITextureSource *getTextureSource() { return m_texturesrc; }
//    IShaderSource *getShaderSource() { return m_shadersrc; }
//    ISoundManager *getSoundManager() { return m_soundmgr; }
//    MtEventManager *getEventManager() { return m_eventmgr; }
//    scene::ISceneManager *getSceneManager() { return m_scenemgr; }
//    IRollbackManager *getRollbackManager() { return m_rollbackmgr; }
//    EmergeManager *getEmergeManager() { return m_emergemgr; }
//
//    scene::IAnimatedMesh *getMesh(const std::string &filename) { return NULL; }
//    bool checkLocalPrivilege(const std::string &priv) { return false; }
//    u16 allocateUnknownNodeId(const std::string &name) { return 0; }
//
//    void defineSomeNodes();
//
//    virtual const std::vector<ModSpec> &getMods() const
//    {
//    static std::vector<ModSpec> testmodspec;
//    return testmodspec;
//    }
//    virtual const ModSpec* getModSpec(const std::string &modname) const { return NULL; }
//    virtual std::string getModStoragePath() const { return "."; }
//    virtual bool registerModStorage(ModMetadata *meta) { return true; }
//    virtual void unregisterModStorage(const std::string &name) {}
//
//    private:
//    IItemDefManager *m_itemdef;
//    INodeDefManager *m_nodedef;
//    ICraftDefManager *m_craftdef;
//    ITextureSource *m_texturesrc;
//    IShaderSource *m_shadersrc;
//    ISoundManager *m_soundmgr;
//    MtEventManager *m_eventmgr;
//    scene::ISceneManager *m_scenemgr;
//    IRollbackManager *m_rollbackmgr;
//    EmergeManager *m_emergemgr;
}

class TestBase
{
    private var context: GlobalContext

    init(testManager: TestManager)
    {
        context = testManager.context
    }
    /// Run all the tests in this `TestBase`
    ///
    /// - Parameter gameDef: Game definition to use for the tests
    /// - Returns: true if all tests succeeded
    func testModule(gameDef: GameDef) -> Bool
    {
        numTestsFailed = 0
        numTestsRun = 0

		print("======== Testing module \(name)")
        let t1 = DispatchTime.now()
        runTests(gameDef: gameDef)
        let tDiffNanoSeconds = DispatchTime.now().rawValue - t1.rawValue

        let failOrPass = numTestsFailed > 0 ? "failed" : "passed"
        print("======== Module \(name) \(failOrPass) (\(numTestsFailed) fails of \(numTestsRun) tests) \(tDiffNanoSeconds / 1_000_000) ms")
        do
        {
            try FileManager.default.removeItem(at: testDir)
        }
        catch
        {
            log.warn("Unable to delete test directory, reason: \(error)")
        }
        return numTestsFailed == 0
    }

    var testHasAsserted: Bool = false
    func test(_ theTest: () throws -> ())
    {
        testHasAsserted = false
		do
        {
            try theTest()
        }
        catch
        {
            log.error("\(error)")
            testHasAsserted = true
        }
        if testHasAsserted
        {
            numTestsFailed += 1
        }
        numTestsRun += 1
    }

    func testAssert(_ condition: Bool,
                    _   message: @autoclosure () -> String,
                           file: String = #file,
                       function: String = #function,
                           line: Int = #line)
    {
		if !condition
        {
            log.error("\(file):\(line): warning: \(function) - assert failed: \(message())")
            testHasAsserted = true
        }
        else if log.isDebug
        {
            log.debug("\(file):\(line): debug: \(function) - assertion passed ")
        }
    }

    func testAssert(_ condition: Bool, file: String = #file, function: String = #function, line: Int = #line)
    {
        testAssert(condition, "", file: file, function: function, line: line)
    }

    func testFail(_ message: @autoclosure () -> String,
                    file: String = #file,
                    function: String = #function,
                    line: Int = #line)
    {
        testAssert(false, message, file: file, function: function, line: line)
    }


    /// Runs a block and test asserts if it throws an exception.
    ///
    /// - Parameters:
    ///   - message: Message to print if the block asserts. Will also print the
    ///              error.
    ///   - file: File name to print as part of the assertion, defaults to the
    ///           current file
    ///   - function: Function to print, defaults to the current function
    ///   - line: Line to print, defaults to the line number of the test
    ///   - code: The code block to execute
    /// - Returns: the return value from the block  if no error was thrown or
    ///            nil if it is.
    @discardableResult
    func testDoesNotThrow<T>(_ message: @autoclosure () -> String,
                               file: String = #file,
                           function: String = #function,
                               line: Int = #line,
                               code: () throws -> T?) -> T?
    {
        do
        {
			return try code()
        }
        catch
        {
			let userMessage = message()
            testFail("\(userMessage), error is '\(error)'")
            return nil
        }
    }


    /// Run the tests for the module. This needs to be overridden by subclasses
    ///
    /// - Parameter gameDef: The game definition
    func runTests(gameDef: GameDef)
    {
        test
        {
            log.error("No unit tests defined for \(name)")
            testFail("No tests defined for this test module")
        }
    }

    /// The name of the test module, must be overridden by subclasses
    var name: String { fatalError("name should be overridden") }
    var numTestsFailed = 0
    var numTestsRun = 0

    private var _testDir: URL? = nil
    private var testDir: URL
    {
        if _testDir == nil
        {
            let lastComponent = String(format: "%08lx.tmp", UInt64(context.pcgRand.zeroBasedRandom(Int(Int32.max))))
            _testDir = FileManager.default.temporaryDirectory.appendingPathComponent(lastComponent)
            // TODO: Handle the error properly
            do
            {
                try FileManager.default.createDirectory(at: _testDir!, withIntermediateDirectories: true, attributes: nil)
            }
            catch
            {
                fatalError("Failed to create test directory, reason: \(error)")
            }
        }
        return _testDir!
    }
}


/// Test manager for running the buit in unit tests.
public struct TestManager
{
    var context: GlobalContext

    public init(context: GlobalContext)
    {
        self.context = context
        testModules = [
            TestAreaStore(testManager: self),
            TestCollision(testManager: self),
        ]
    }

    private var testModules: [TestBase] = []

    public func runUnitTests() -> Int
    {

        let t1 = DispatchTime.now()
        let gameDef = TestGameDef()

        var numModulesFailed = 0
        var totalNumTestsFailed = 0
        var totalNumTestsRun = 0

        for test in testModules
        {
            if !test.testModule(gameDef: gameDef)
            {
                numModulesFailed += 1
            }
            totalNumTestsFailed += test.numTestsFailed
            totalNumTestsRun += test.numTestsRun
        }
        let tDiffNanoSeconds = DispatchTime.now().rawValue - t1.rawValue
        let overallStatus = numModulesFailed > 0 ? "FAILED" : "PASSED"
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print("Unit test results: \(overallStatus)")
        print("    \(numModulesFailed) / \(testModules.count) failed modules (\(totalNumTestsFailed) / \(totalNumTestsRun) failed tests)")
        print("    Testing took \(tDiffNanoSeconds / 1_000_000) ms")
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        return numModulesFailed
    }
}

class NoTest: TestBase
{
    override var name: String { return "No Test" }
    override func runTests(gameDef: GameDef)
    {
        test {
            testAssert(false, "This should assert")
        }
        test {
            log.info("A test that passes")
        }
    }
}
