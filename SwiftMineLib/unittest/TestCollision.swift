//
//  TestCollision.swift
//  SwiftMineLib
//
//  Created by Jeremy Pereira on 08/06/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//

import Toolbox

class TestCollision: TestBase
{
    override var name: String { return "Collision" }

    override func runTests(gameDef: GameDef)
    {
//        Logger.pushLevel(.debug, forName: "SwiftMineLib.unittest.Test")
//        defer { Logger.popLevel(forName: "SwiftMineLib.unittest.Test") }
        test(axisAlignedCollision)
    }



    func axisAlignedCollision()
    {
        func shouldHitTest(s: AABBox3DF,
                           m: AABBox3DF,
                           v: Vector3D<Float>,
           expectedCollision: Collision,
                        line: Int = #line)
        {
            if let collision = Collision.axisAlignedCollision(staticBox: s,
                                                              movingBox: m,
                                                               velocity: v,
                                                                  delta: 0)
            {
                testAssert(collision.direction == expectedCollision.direction, "Wrong direction", line: line)
                testAssert(fabsf(collision.deltaTime - expectedCollision.deltaTime) < 0.001,
                           "Wrong deltaTime \(collision.deltaTime), expected \(expectedCollision.deltaTime)",
                           line: line)
            }
            else
            {
                testFail("Failed to detect collision for s=\(s), m=\(m), v=\(v)", line: line)
            }
        }
        func shouldMissTest(s: AABBox3DF,
                            m: AABBox3DF,
                            v: Vector3D<Float>,
                         line: Int = #line)
        {
            if let collision = Collision.axisAlignedCollision(staticBox: s,
                                                              movingBox: m,
                                                              velocity: v,
                                                              delta: 0)
            {
                testFail("Collision should not have been detected in direction \(collision.direction)")
            }
        }

        for bx in stride(from: Float(-3), to: 3, by: 1)
        {
            for by in stride(from: Float(-3), to: 3, by: 1)
            {
                for bz in stride(from: Float(-3), to: 3, by: 1)
                {
                    // Test for psoitve X velocity collision
                    var s = AABBox3DF(minX: bx, minY: by, minZ: bz,
                                      maxX: bx + 1, maxY: by + 1, maxZ: bz + 1)
                    let m = AABBox3DF(minX: bx - 2, minY: by, minZ: bz,
                                      maxX: bx - 1, maxY: by + 1, maxZ: bz + 1)
                    var v = Vector3D<Float>(x: 1, y: 0, z: 0)
                    shouldHitTest(s: s, m: m, v: v, expectedCollision: Collision(direction: .x, deltaTime: 1))
                    // Test for no collision
					v = Vector3D<Float>(x: -1, y: 0, z: 0)
                    shouldMissTest(s: s, m: m, v: v)

                    // Collision in right direction but boxes should miss each other
                    shouldMissTest(s: s,
                                   m: AABBox3DF(minX: bx - 2, minY: by + 1.5, minZ: bz,
                                                maxX: bx - 1, maxY: by + 2.5, maxZ: bz - 1),
                                   v: Vector3D<Float>(x: 1, y: 0, z: 0))

                    // Same as last test but velocity should cause collision
                    shouldHitTest(s: s,
                                  m: AABBox3DF(minX: bx - 2, minY: by - 1.5, minZ: bz,
                                               maxX: bx - 1.5, maxY: by + 0.5, maxZ: bz + 1),
                                  v: Vector3D<Float>(x: 0.5, y: 0.1, z: 0),
                  expectedCollision: Collision(direction: .x, deltaTime: 3))

                    shouldHitTest(s: s,
                                  m: AABBox3DF(minX: bx + 2, minY: by, minZ: bz,
                                               maxX: bx + 3, maxY: by + 1, maxZ: bz + 1),
                                  v: Vector3D<Float>(x: -1, y: 0, z: 0),
                                  expectedCollision: Collision(direction: .x, deltaTime: 1))
                    shouldMissTest(s: s,
                                   m: AABBox3DF(minX: bx + 2, minY: by, minZ: bz,
                                                maxX: bx + 3, maxY: by + 1, maxZ: bz + 1),
                                   v: Vector3D<Float>(x: 1, y: 0, z: 0))
                    shouldMissTest(s: s,
                                   m: AABBox3DF(minX: bx + 2, minY: by, minZ: bz + 1.5,
                                                maxX: bx + 3, maxY: by + 1, maxZ: bz + 3.5),
                                   v: Vector3D<Float>(x: -1, y: 0, z: 0))
                    shouldHitTest(s: s,
                                  m: AABBox3DF(minX: bx + 2, minY: by - 1.5, minZ: bz,
                                               maxX: bx + 2.5, maxY: by - 0.5, maxZ: bz + 1),
                                  v: Vector3D<Float>(x: -0.5, y: 0.2, z: 0),
                  expectedCollision: Collision(direction: .y, deltaTime: 2.5))
                    shouldHitTest(s: s,
                                  m: AABBox3DF(minX: bx + 2, minY: by - 1.5, minZ: bz,
                                               maxX: bx + 2.5, maxY: by - 0.5, maxZ: bz + 1),
                                  v: Vector3D<Float>(x: -0.5, y: 0.3, z: 0),
                  expectedCollision: Collision(direction: .x, deltaTime: 2))
                // TODO: Y-, Y+, Z-, Z+ (MineTest has the same TODO

                    s = AABBox3DF(minX: bx, minY: by, minZ: bz,
                                  maxX: bx + 2, maxY: by + 2, maxZ: bz + 2)
                    shouldHitTest(s: s,
                                  m: AABBox3DF(minX: bx + 2.3, minY: by + 2.29, minZ: bz + 2.29,
                                               maxX: bx + 4.2, maxY: by + 4.2, maxZ: bz + 4.2),
                                  v: Vector3D<Float>(x: -1.0/3, y: -1.0/3, z: -1.0/3),
                  expectedCollision: Collision(direction: .x, deltaTime: 0.9))
                    shouldHitTest(s: s,
                                  m: AABBox3DF(minX: bx + 2.29, minY: by + 2.3, minZ: bz + 2.29,
                                               maxX: bx + 4.2, maxY: by + 4.2, maxZ: bz + 4.2),
                                  v: Vector3D<Float>(x: -1.0/3, y: -1.0/3, z: -1.0/3),
                                  expectedCollision: Collision(direction: .y, deltaTime: 0.9))
                    shouldHitTest(s: s,
                                  m: AABBox3DF(minX: bx + 2.29, minY: by + 2.29, minZ: bz + 2.3,
                                               maxX: bx + 4.2, maxY: by + 4.2, maxZ: bz + 4.2),
                                  v: Vector3D<Float>(x: -1.0/3, y: -1.0/3, z: -1.0/3),
                                  expectedCollision: Collision(direction: .z, deltaTime: 0.9))
                    shouldHitTest(s: s,
                                  m: AABBox3DF(minX: bx - 4.2, minY: by - 4.2, minZ: bz - 4.2,
                                               maxX: bx - 2.3, maxY: by - 2.29, maxZ: bz - 2.29),
                                  v: Vector3D<Float>(x: 1.0/7, y: 1.0/7, z: 1.0/7),
                                  expectedCollision: Collision(direction: .x, deltaTime: 16.1))
                    shouldHitTest(s: s,
                                  m: AABBox3DF(minX: bx - 4.2, minY: by - 4.2, minZ: bz - 4.2,
                                               maxX: bx - 2.29, maxY: by - 2.3, maxZ: bz - 2.29),
                                  v: Vector3D<Float>(x: 1.0/7, y: 1.0/7, z: 1.0/7),
                                  expectedCollision: Collision(direction: .y, deltaTime: 16.1))
                    shouldHitTest(s: s,
                                  m: AABBox3DF(minX: bx - 4.2, minY: by - 4.2, minZ: bz - 4.2,
                                               maxX: bx - 2.29, maxY: by - 2.29, maxZ: bz - 2.3),
                                  v: Vector3D<Float>(x: 1.0/7, y: 1.0/7, z: 1.0/7),
                                  expectedCollision: Collision(direction: .z, deltaTime: 16.1))
                }
            }
        }
    }
}
