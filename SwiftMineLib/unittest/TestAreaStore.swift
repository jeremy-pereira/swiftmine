//
//  TestAreaStore.swift
//  SwiftMineLib
//
//  Created by Jeremy Pereira on 24/05/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//
import Toolbox

private let log = Logger.getLogger("SwiftMineLib.unittest.TestAreaStore")

class TestAreaStore: TestBase
{
    override var name: String { return "AreaStore" }
    override func runTests(gameDef: GameDef)
    {
        test(testVectorStore)
        test(testSpatialStore)
        test(testSerialisation)
    }

    func testVectorStore()
    {
        let store = VectorAreaStore()
        genericTest(store: store)
    }

    func testSpatialStore()
    {
        //let store = SpatialAreaStore()
        // TODO: Implement the spatial index test
 //       genericTest(store: store)
    }


    let expectedSerialisedString = """
    {"areaMap":[0,{"id":0,"maxEdge":{"x":0,"y":1,"z":2},"data":"Area a","minEdge":{"x":-1,"y":0,"z":1}},1,{"id":1,"maxEdge":{"x":32000,"y":456,"z":789},"data":"Area B","minEdge":{"x":123,"y":100,"z":10}}],"cacheBlockRadius":64,"nextId":2,"cacheIsEnabled":true}
    """

    func testSerialisation()
    {
        log.pushLevel(.debug)
        defer { log.popLevel() }

        let store = VectorAreaStore()
        let a = Area(minEdge: V3S16(x: -1, y: 0, z: 1),
                     maxEdge: V3S16(x: 0, y: 1, z: 2))
        a.data = "Area a"
        try! store.insert(area: a)

        let b = Area(minEdge: V3S16(x: 123, y: 456, z: 789), maxEdge: V3S16(x: 32000, y: 100, z: 10))
        b.data = "Area B"
        try! store.insert(area: b)
        let str: String? = testDoesNotThrow("Failed to serialse the area store")
        {
            let data = try JSONEncoder().encode(store)
            return String(bytes: data, encoding: String.Encoding.utf8)
        }
        guard let serialised = str
        else
        {
			testFail("No string returned")
            return
        }
        log.debug("Serialisiation is '\(serialised)'")
		testAssert(serialised == expectedSerialisedString, "Serialisation wrong")

        guard let newStore = testDoesNotThrow("Failed to decode area store", code:
        {
            return try JSONDecoder().decode(VectorAreaStore.self, from: serialised.data(using: .utf8)!)
        })
        else { return }

        testAssert(newStore.count == 2, "Wrong count from deserialisation")
//
//        std::istringstream is(str);
//        store.deserialize(is);
//
//        UASSERTEQ(size_t, store.size(), 4);  // deserialize() doesn't clear the store
    }

    private func genericTest(store: AreaStore)
    {
        let a = Area(minEdge: V3S16(x: -10, y: -3, z: 5), maxEdge: V3S16(x: 0, y: 29, z: 7))
        let b = Area(minEdge: V3S16(x: -5, y: -2, z: 5), maxEdge: V3S16(x: 0, y: 28, z: 6))
        let c = Area(minEdge: V3S16(x: -7, y: -3, z: 6), maxEdge: V3S16(x: -1, y: 27, z: 7))

        var res: [Area]
        var noError: Bool?

        testAssert(store.count == 0)
        store.reserve(count: 2)
        noError = testDoesNotThrow("Failed to insert")
        {
            try store.insert(area: a)
            try store.insert(area: b)
            try store.insert(area: c)
            return true
        }
        guard noError != nil else { return }
        testAssert(store.count == 3, "Wrong store count \(store.count)")

        res = store.areasFor(position: V3S16(x: -1, y: 0, z: 6))
        testAssert(res.count == 3, "Wrong area count, got \(res.count)")

        res = store.areasFor(position: V3S16(x: 0, y: 0, z: 7))
        testAssert(res.count == 1, "Wrong area count, got \(res.count)")

        testAssert(store.removeArea(id: a.id), "Area was not in the store")
        res = store.areasFor(position: V3S16(x: 0, y: 0, z: 7))
        testAssert(res.count == 0, "Wrong area count, got \(res.count)")

        noError = testDoesNotThrow("Failed to insert")
        {
            try store.insert(area: a)
            return true
        }
        guard noError != nil else { return }

        res = store.areasFor(position: V3S16(x: 0, y: 0, z: 7))
        testAssert(res.count == 1, "Wrong area count, got \(res.count)")
        res = store.areasInArea(minEdge: V3S16(x: -10, y: -3, z: 5),
                                maxEdge: V3S16(x: 0, y: 29, z: 7),
                                acceptOverlap: false)
        testAssert(res.count == 3, "Wrong area count, got \(res.count)")
        res = store.areasInArea(minEdge: V3S16(x: -100, y: 0, z: 6),
                                maxEdge: V3S16(x: 200, y: 0, z: 6),
                                acceptOverlap: false)
        testAssert(res.count == 0, "Wrong area count, got \(res.count)")

        res = store.areasInArea(minEdge: V3S16(x: -100, y: 0, z: 6),
                                maxEdge: V3S16(x: 200, y: 0, z: 6),
                          acceptOverlap: true)
        testAssert(res.count == 3, "Wrong area count, got \(res.count)")

        testAssert(store.removeArea(id: a.id), "a was not in the store")
        testAssert(store.removeArea(id: b.id), "b was not in the store")
        testAssert(store.removeArea(id: c.id), "c was not in the store")

        let d = Area(minEdge: V3S16(x: -100, y: -300, z: -200),
                     maxEdge: V3S16(x: -50, y: -200, z: -100))
        d.data = "Hello, World!"
        noError = testDoesNotThrow("Could not insert d")
        {
            try store.insert(area: d)
            return true
        }
        guard noError != nil else { return }

        res = store.areasFor(position: V3S16(x: -75, y: -250, z: -150))
        testAssert(res.count == 1, "Failed to find d area")
        if res.count > 0
        {
            testAssert(res[0].data == "Hello, World!", "d has wrong data")
        }

        testAssert(store.removeArea(id: d.id), "d was not in the store")
    }
}
