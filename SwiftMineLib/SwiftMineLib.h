//
//  SwiftMineLib.h
//  SwiftMineLib
//
//  Created by Jeremy Pereira on 18/05/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for SwiftMineLib.
FOUNDATION_EXPORT double SwiftMineLibVersionNumber;

//! Project version string for SwiftMineLib.
FOUNDATION_EXPORT const unsigned char SwiftMineLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SwiftMineLib/PublicHeader.h>


