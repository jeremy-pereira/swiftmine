//
//  socket.swift
//  SwiftMineLib
//
//  Created by Jeremy Pereira on 19/05/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//


/// The socket subsyatem
public struct Socket
{

    /// Has the socket subsystem been initialised?
    public private(set) static var initialised = false

    /// Inirtialsie the socket subsystem. Probably there is nothing to do.
    public static func start()
    {
        initialised = true
    }

    /// Do any socket clean up needed. Note that this runs in the OS `atexit`
    /// handler.
    public static func cleanUp()
    {
        initialised = false
    }
}
