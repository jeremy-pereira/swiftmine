//
//  Porting.swift
//  SwiftMineLib
//
//  Created by Jeremy Pereira on 18/05/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//
import Foundation
import Toolbox

private let log = Logger.getLogger("SwiftMineLib.Porting")

private func signalHandler(_ sigNum: Int32) -> Void
{
    if !Porting.killed
    {
        // TODO: Somehow log which signal we got
        Porting.killed = true
    }
    else
    {
        signal(sigNum, SIG_DFL)
    }
}


/// An abstraction layer for OS interfaces
public struct Porting
{
	public init()
    {
    }
    /// Errors associated with porting
    ///
    /// - noResourceURL: Cannot find the resource URL in the main bundle
    public enum Err: Error
    {
        case noResourceURL
    }
//
//    /*
//     Signal handler (grabs Ctrl-C on POSIX systems)
//     */
//
//    bool g_killed = false;
//
//    bool * signal_handler_killstatus(void)
//    {
//    return &g_killed;
//    }
//
//    #if !defined(_WIN32) // POSIX
//    #include <signal.h>
//
    fileprivate static var killed: Bool = false


    /// Installs signal handlers for `SIGINT` and `SIGTERM` i.e. `ctrl-c` and
    /// the shell `kill`
    public static func signalHandlerInit()
    {
		signal(SIGINT, signalHandler)
        signal(SIGTERM, signalHandler)
    }
//
//    /*
//     Path mangler
//     */
//
//
    // Default to RUN_IN_PLACE style relative paths
    /// The path containing the applications resources
    public private(set) var sharePath = URL(fileURLWithPath: "..")
    /// The path for user data
    public private(set) var userPath = URL(fileURLWithPath: "..")
    /// The user's cache path
    public var cachePath: URL { return userPath.appendingPathComponent("cache") }
//    std::string path_locale = path_share + DIR_DELIM + "locale";
//
//
//    std::string getDataPath(const char *subpath)
//    {
//    return path_share + DIR_DELIM + subpath;
//    }
//
//    void pathRemoveFile(char *path, char delim)
//    {
//    // Remove filename and path delimiter
//    int i;
//    for(i = strlen(path)-1; i>=0; i--)
//    {
//    if(path[i] == delim)
//    break;
//    }
//    path[i] = 0;
//    }
//
//    bool detectMSVCBuildDir(const std::string &path)
//    {
//    const char *ends[] = {
//    "bin\\Release",
//    "bin\\MinSizeRel",
//    "bin\\RelWithDebInfo",
//    "bin\\Debug",
//    "bin\\Build",
//    NULL
//    };
//    return (removeStringEnd(path, ends) != "");
//    }
//
//    std::string get_sysinfo()
//    {
//    #ifdef _WIN32
//    OSVERSIONINFO osvi;
//    std::ostringstream oss;
//    std::string tmp;
//    ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
//    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
//    GetVersionEx(&osvi);
//    tmp = osvi.szCSDVersion;
//    std::replace(tmp.begin(), tmp.end(), ' ', '_');
//
//    oss << "Windows/" << osvi.dwMajorVersion << "."
//    << osvi.dwMinorVersion;
//    if (osvi.szCSDVersion[0])
//    oss << "-" << tmp;
//    oss << " ";
//    #ifdef _WIN64
//    oss << "x86_64";
//    #else
//    BOOL is64 = FALSE;
//    if (IsWow64Process(GetCurrentProcess(), &is64) && is64)
//    oss << "x86_64"; // 32-bit app on 64-bit OS
//    else
//    oss << "x86";
//    #endif
//
//    return oss.str();
//    #else
//    struct utsname osinfo;
//    uname(&osinfo);
//    return std::string(osinfo.sysname) + "/"
//    + osinfo.release + " " + osinfo.machine;
//    #endif
//    }
//
//
//    bool getCurrentWorkingDir(char *buf, size_t len)
//    {
//    #ifdef _WIN32
//    DWORD ret = GetCurrentDirectory(len, buf);
//    return (ret != 0) && (ret <= len);
//    #else
//    return getcwd(buf, len);
//    #endif
//    }
//
//
//    bool getExecPathFromProcfs(char *buf, size_t buflen)
//    {
//    #ifndef _WIN32
//    buflen--;
//
//    ssize_t len;
//    if ((len = readlink("/proc/self/exe",     buf, buflen)) == -1 &&
//    (len = readlink("/proc/curproc/file", buf, buflen)) == -1 &&
//    (len = readlink("/proc/curproc/exe",  buf, buflen)) == -1)
//    return false;
//
//    buf[len] = '\0';
//    return true;
//    #else
//    return false;
//    #endif
//    }
//
//    //// Windows
//    #if defined(_WIN32)
//
//    bool getCurrentExecPath(char *buf, size_t len)
//    {
//    DWORD written = GetModuleFileNameA(NULL, buf, len);
//    if (written == 0 || written == len)
//    return false;
//
//    return true;
//    }
//
//
//    //// Linux
//    #elif defined(__linux__)
//
//    bool getCurrentExecPath(char *buf, size_t len)
//    {
//    if (!getExecPathFromProcfs(buf, len))
//    return false;
//
//    return true;
//    }
//
//
//    //// Mac OS X, Darwin
//    #elif defined(__APPLE__)
//
//    bool getCurrentExecPath(char *buf, size_t len)
//    {
//    uint32_t lenb = (uint32_t)len;
//    if (_NSGetExecutablePath(buf, &lenb) == -1)
//    return false;
//
//    return true;
//    }
//
//
//    //// FreeBSD, NetBSD, DragonFlyBSD
//    #elif defined(__FreeBSD__) || defined(__NetBSD__) || defined(__DragonFly__)
//
//    bool getCurrentExecPath(char *buf, size_t len)
//    {
//    // Try getting path from procfs first, since valgrind
//    // doesn't work with the latter
//    if (getExecPathFromProcfs(buf, len))
//    return true;
//
//    int mib[4];
//
//    mib[0] = CTL_KERN;
//    mib[1] = KERN_PROC;
//    mib[2] = KERN_PROC_PATHNAME;
//    mib[3] = -1;
//
//    if (sysctl(mib, 4, buf, &len, NULL, 0) == -1)
//    return false;
//
//    return true;
//    }
//
//
//    //// Solaris
//    #elif defined(__sun) || defined(sun)
//
//    bool getCurrentExecPath(char *buf, size_t len)
//    {
//    const char *exec = getexecname();
//    if (exec == NULL)
//    return false;
//
//    if (strlcpy(buf, exec, len) >= len)
//    return false;
//
//    return true;
//    }
//
//
//    // HP-UX
//    #elif defined(__hpux)
//
//    bool getCurrentExecPath(char *buf, size_t len)
//    {
//    struct pst_status psts;
//
//    if (pstat_getproc(&psts, sizeof(psts), 0, getpid()) == -1)
//    return false;
//
//    if (pstat_getpathname(buf, len, &psts.pst_fid_text) == -1)
//    return false;
//
//    return true;
//    }
//
//
//    #else
//
//    bool getCurrentExecPath(char *buf, size_t len)
//    {
//    return false;
//    }
//
//    #endif
//
//

//    //// Mac OS X
//    #elif defined(__APPLE__)
//
    private mutating func setSystemPaths() throws
    {
		let bundle = Bundle.main
        guard let resourceUrl = bundle.resourceURL else { throw Err.noResourceURL }

		sharePath = resourceUrl
		userPath = try FileManager.default.url(for: .applicationSupportDirectory,
                                                in: FileManager.SearchPathDomainMask.userDomainMask,
                                    appropriateFor: nil,
                                            create: true).appendingPathComponent("SwiftMine")
    }

	public mutating func initialisePaths() throws
    {
        // TODO: Set up something for run in place

        log.info("Using system-wide paths (NOT RUN_IN_PLACE)")

		try setSystemPaths()
        if !cachePath.hasDirectoryPath
        {
            try FileManager.default.createDirectory(at: cachePath,
                           withIntermediateDirectories: true,
                                            attributes: nil)
        }
        log.info("share path is \(sharePath)")
        log.info("user path is \(userPath)")
        log.info("cache path is \(cachePath)")
        // TODO: might need to set up some gettext locale stuff here
    }


//
//
//
//void setXorgClassHint(const video::SExposedVideoData &video_data,
//    const std::string &name)
//{
//    #ifdef XORG_USED
//    if (video_data.OpenGLLinux.X11Display == NULL)
//    return;
//
//    XClassHint *classhint = XAllocClassHint();
//    classhint->res_name  = (char *)name.c_str();
//    classhint->res_class = (char *)name.c_str();
//
//    XSetClassHint((Display *)video_data.OpenGLLinux.X11Display,
//                  video_data.OpenGLLinux.X11Window, classhint);
//    XFree(classhint);
//#endif
//}
//
//bool setWindowIcon(IrrlichtDevice *device)
//{
//    #if defined(XORG_USED)
//    #    if RUN_IN_PLACE
//    return setXorgWindowIconFromPath(device,
//                                     path_share + "/misc/" PROJECT_NAME "-xorg-icon-128.png");
//    #    else
//    // We have semi-support for reading in-place data if we are
//    // compiled with RUN_IN_PLACE. Don't break with this and
//    // also try the path_share location.
//    return
//        setXorgWindowIconFromPath(device,
//                                  ICON_DIR "/hicolor/128x128/apps/" PROJECT_NAME ".png") ||
//            setXorgWindowIconFromPath(device,
//                                      path_share + "/misc/" PROJECT_NAME "-xorg-icon-128.png");
//    #    endif
//    #elif defined(_WIN32)
//    const video::SExposedVideoData exposedData = device->getVideoDriver()->getExposedVideoData();
//    HWND hWnd; // Window handle
//
//    switch (device->getVideoDriver()->getDriverType()) {
//    case video::EDT_DIRECT3D8:
//    hWnd = reinterpret_cast<HWND>(exposedData.D3D8.HWnd);
//    break;
//    case video::EDT_DIRECT3D9:
//    hWnd = reinterpret_cast<HWND>(exposedData.D3D9.HWnd);
//    break;
//    case video::EDT_OPENGL:
//    hWnd = reinterpret_cast<HWND>(exposedData.OpenGLWin32.HWnd);
//    break;
//    default:
//        return false;
//    }
//
//    // Load the ICON from resource file
//    const HICON hicon = LoadIcon(
//    GetModuleHandle(NULL),
//    MAKEINTRESOURCE(130) // The ID of the ICON defined in winresource.rc
//    );
//
//    if (hicon) {
//        SendMessage(hWnd, WM_SETICON, ICON_BIG, reinterpret_cast<LPARAM>(hicon));
//        SendMessage(hWnd, WM_SETICON, ICON_SMALL, reinterpret_cast<LPARAM>(hicon));
//        return true;
//    }
//    return false;
//    #else
//    return false;
//    #endif
//}
//
//bool setXorgWindowIconFromPath(IrrlichtDevice *device,
//                               const std::string &icon_file)
//{
//    #ifdef XORG_USED
//
//    video::IVideoDriver *v_driver = device->getVideoDriver();
//
//    video::IImageLoader *image_loader = NULL;
//    u32 cnt = v_driver->getImageLoaderCount();
//    for (u32 i = 0; i < cnt; i++) {
//        if (v_driver->getImageLoader(i)->isALoadableFileExtension(icon_file.c_str())) {
//            image_loader = v_driver->getImageLoader(i);
//            break;
//        }
//    }
//
//    if (!image_loader) {
//        warningstream << "Could not find image loader for file '"
//            << icon_file << "'" << std::endl;
//        return false;
//    }
//
//    io::IReadFile *icon_f = device->getFileSystem()->createAndOpenFile(icon_file.c_str());
//
//    if (!icon_f) {
//        warningstream << "Could not load icon file '"
//            << icon_file << "'" << std::endl;
//        return false;
//    }
//
//    video::IImage *img = image_loader->loadImage(icon_f);
//
//    if (!img) {
//        warningstream << "Could not load icon file '"
//            << icon_file << "'" << std::endl;
//        icon_f->drop();
//        return false;
//    }
//
//    u32 height = img->getDimension().Height;
//    u32 width = img->getDimension().Width;
//
//    size_t icon_buffer_len = 2 + height * width;
//    long *icon_buffer = new long[icon_buffer_len];
//
//    icon_buffer[0] = width;
//    icon_buffer[1] = height;
//
//    for (u32 x = 0; x < width; x++) {
//        for (u32 y = 0; y < height; y++) {
//            video::SColor col = img->getPixel(x, y);
//            long pixel_val = 0;
//            pixel_val |= (u8)col.getAlpha() << 24;
//            pixel_val |= (u8)col.getRed() << 16;
//            pixel_val |= (u8)col.getGreen() << 8;
//            pixel_val |= (u8)col.getBlue();
//            icon_buffer[2 + x + y * width] = pixel_val;
//        }
//    }
//
//    img->drop();
//    icon_f->drop();
//
//    const video::SExposedVideoData &video_data = v_driver->getExposedVideoData();
//
//    Display *x11_dpl = (Display *)video_data.OpenGLLinux.X11Display;
//
//    if (x11_dpl == NULL) {
//        warningstream << "Could not find x11 display for setting its icon."
//            << std::endl;
//        delete [] icon_buffer;
//        return false;
//    }
//
//    Window x11_win = (Window)video_data.OpenGLLinux.X11Window;
//
//    Atom net_wm_icon = XInternAtom(x11_dpl, "_NET_WM_ICON", False);
//    Atom cardinal = XInternAtom(x11_dpl, "CARDINAL", False);
//    XChangeProperty(x11_dpl, x11_win,
//    net_wm_icon, cardinal, 32,
//    PropModeReplace, (const unsigned char *)icon_buffer,
//    icon_buffer_len);
//
//    delete [] icon_buffer;
//
//#endif
//return true;
//}
//
//////
////// Video/Display Information (Client-only)
//////
//
//#ifndef SERVER
//
//static irr::IrrlichtDevice *device;
//
//void initIrrlicht(irr::IrrlichtDevice *device_)
//{
//    device = device_;
//}
//
//v2u32 getWindowSize()
//    {
//        return device->getVideoDriver()->getScreenSize();
//}
//
//
//std::vector<core::vector3d<u32> > getSupportedVideoModes()
//{
//    IrrlichtDevice *nulldevice = createDevice(video::EDT_NULL);
//    sanity_check(nulldevice != NULL);
//
//    std::vector<core::vector3d<u32> > mlist;
//    video::IVideoModeList *modelist = nulldevice->getVideoModeList();
//
//    u32 num_modes = modelist->getVideoModeCount();
//    for (u32 i = 0; i != num_modes; i++) {
//        core::dimension2d<u32> mode_res = modelist->getVideoModeResolution(i);
//        s32 mode_depth = modelist->getVideoModeDepth(i);
//        mlist.push_back(core::vector3d<u32>(mode_res.Width, mode_res.Height, mode_depth));
//    }
//
//    nulldevice->drop();
//
//    return mlist;
//}
//
//std::vector<irr::video::E_DRIVER_TYPE> getSupportedVideoDrivers()
//{
//    std::vector<irr::video::E_DRIVER_TYPE> drivers;
//
//    for (int i = 0; i != irr::video::EDT_COUNT; i++) {
//        if (irr::IrrlichtDevice::isDriverSupported((irr::video::E_DRIVER_TYPE)i))
//        drivers.push_back((irr::video::E_DRIVER_TYPE)i);
//    }
//
//    return drivers;
//}
//
//const char *getVideoDriverName(irr::video::E_DRIVER_TYPE type)
//    {
//        static const char *driver_ids[] = {
//            "null",
//            "software",
//            "burningsvideo",
//            "direct3d8",
//            "direct3d9",
//            "opengl",
//            "ogles1",
//            "ogles2",
//        };
//
//        return driver_ids[type];
//}
//
//
//const char *getVideoDriverFriendlyName(irr::video::E_DRIVER_TYPE type)
//    {
//        static const char *driver_names[] = {
//            "NULL Driver",
//            "Software Renderer",
//            "Burning's Video",
//            "Direct3D 8",
//            "Direct3D 9",
//            "OpenGL",
//            "OpenGL ES1",
//            "OpenGL ES2",
//        };
//
//        return driver_names[type];
//}
//
//#    ifndef __ANDROID__
//#        ifdef XORG_USED
//
//static float calcDisplayDensity()
//{
//    const char *current_display = getenv("DISPLAY");
//
//    if (current_display != NULL) {
//        Display *x11display = XOpenDisplay(current_display);
//
//        if (x11display != NULL) {
//            /* try x direct */
//            float dpi_height = floor(DisplayHeight(x11display, 0) /
//                (DisplayHeightMM(x11display, 0) * 0.039370) + 0.5);
//            float dpi_width = floor(DisplayWidth(x11display, 0) /
//                (DisplayWidthMM(x11display, 0) * 0.039370) + 0.5);
//
//            XCloseDisplay(x11display);
//
//            return std::max(dpi_height,dpi_width) / 96.0;
//        }
//    }
//
//    /* return manually specified dpi */
//    return g_settings->getFloat("screen_dpi")/96.0;
//}
//
//
//float getDisplayDensity()
//    {
//        static float cached_display_density = calcDisplayDensity();
//        return cached_display_density;
//}
//
//
//#        else // XORG_USED
//float getDisplayDensity()
//{
//    return g_settings->getFloat("screen_dpi")/96.0;
//}
//#        endif // XORG_USED
//
//v2u32 getDisplaySize()
//{
//    IrrlichtDevice *nulldevice = createDevice(video::EDT_NULL);
//
//    core::dimension2d<u32> deskres = nulldevice->getVideoModeList()->getDesktopResolution();
//    nulldevice -> drop();
//
//    return deskres;
//}
//#    endif // __ANDROID__
//#endif // SERVER
//
//
//////
////// OS-specific Secure Random
//////
//
//#ifdef WIN32
//
//bool secure_rand_fill_buf(void *buf, size_t len)
//{
//    HCRYPTPROV wctx;
//
//    if (!CryptAcquireContext(&wctx, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
//    return false;
//
//    CryptGenRandom(wctx, len, (BYTE *)buf);
//    CryptReleaseContext(wctx, 0);
//    return true;
//}
//
//#else
//
//bool secure_rand_fill_buf(void *buf, size_t len)
//{
//    // N.B.  This function checks *only* for /dev/urandom, because on most
//    // common OSes it is non-blocking, whereas /dev/random is blocking, and it
//    // is exceptionally uncommon for there to be a situation where /dev/random
//    // exists but /dev/urandom does not.  This guesswork is necessary since
//    // random devices are not covered by any POSIX standard...
//    FILE *fp = fopen("/dev/urandom", "rb");
//    if (!fp)
//    return false;
//
//    bool success = fread(buf, len, 1, fp) == 1;
//
//    fclose(fp);
//    return success;
//}
//
//#endif
//
//// Load performance counter frequency only once at startup
//#ifdef _WIN32
//
//inline double get_perf_freq()
//{
//    LARGE_INTEGER freq;
//    QueryPerformanceFrequency(&freq);
//    return freq.QuadPart;
//}
//
//double perf_freq = get_perf_freq();
//
//#endif

}
