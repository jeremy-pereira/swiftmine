#!/bin/sh

dev_dir=$HOME/dev
minetest_dir=/opt/local/var/macports/build/_opt_local_var_macports_sources_rsync.macports.org_release_tarballs_ports_games_minetest/minetest/work/minetest-0.4.16/src
swiftmine_dir=${dev_dir}/SwiftMine

find $minetest_dir -name '*.cpp' -exec wc -l {} \; \
  | sed -e 's/^ *//'	\
  | sed -e "s?${minetest_dir}/??" \
  | sed -e 's/ /	/' \
  > cpp_lines.txt
find $swiftmine_dir -name "*.swift" -exec wc -l {} \; \
  | sed -e 's/^ *//'	\
  | sed -e "s?${swiftmine_dir}/??" \
  | sed -e 's/ /	/' \
  >swift_lines.txt
